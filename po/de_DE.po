# VDR plugin language source file.
msgid ""
msgstr ""
"Project-Id-Version: vdr-tvguide 0.0.1\n"
"Report-Msgid-Bugs-To: <see README>\n"
"POT-Creation-Date: 2022-04-29 16:05+0200\n"
"PO-Revision-Date: 2012-08-25 17:49+0200\n"
"Last-Translator: Horst\n"
"Language-Team: \n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "Main Program"
msgstr "Hauptprogramm"

msgid "Channel"
msgstr "Kanal"

msgid "min"
msgstr "min"

msgid "Reruns of "
msgstr "Wiederholungen von "

msgid "No reruns found"
msgstr "Keine Wiederholungen gefunden"

msgid "No EPG Information available"
msgstr "Keine EPG Daten verfügbar"

msgid "Search & Rec"
msgstr "Suchen & Aufn."

msgid "Channels back"
msgstr "Kanäle zurück"

msgid "Channels forward"
msgstr "Kanäle vor"

msgid "Switch to Channel"
msgstr "Umschalten"

msgid "Detailed EPG"
msgstr "Detailiertes EPG"

msgid "Close detailed EPG"
msgstr "Det. EPG schließen"

msgid "Favorites"
msgstr "Favoriten"

msgid "images"
msgstr "Bilder"

msgid "tvguide: RemoteTimerModifications failed"
msgstr ""

msgid "root video folder"
msgstr "Video Hauptverzeichnis"

msgid "Transp."
msgstr "Transp."

msgid "Timer Conflict"
msgstr "Timer Konflikt"

msgid "all Channels"
msgstr "alle Kanäle"

msgid "unknown channel"
msgstr "unbekannter Kanal"

msgid "with"
msgstr "mit"

msgid "errors"
msgstr "Fehler"

msgid "error"
msgstr "Fehler"

msgid "Duration"
msgstr "Dauer"

msgid "recorded at"
msgstr "aufgenommen am"

msgid "from"
msgstr "von"

msgid "Timers for"
msgstr "Timer für"

msgid "Rec"
msgstr "Aufn"

msgid "No Timers active"
msgstr "Keine Timer aktiv"

msgid "inactive"
msgstr "inaktiv"

msgid "active timers"
msgstr "aktive Timer"

msgid "recordings done"
msgstr "erledigte Aufnahmen"

msgid "Instant Record"
msgstr "Aufnahme"

msgid "Timer On/Off"
msgstr "Timer Ein/Aus"

msgid "Edit Timer"
msgstr "Timer bearbeiten"

msgid "Delete Timer"
msgstr "Timer löschen"

msgid "Timer Timeline"
msgstr "Timer Tagesübersicht"

msgid "Create Search Timer"
msgstr "Suchtimer anlegen"

msgid "Search Timers"
msgstr "Suchtimer"

msgid "Create Series Timer"
msgstr "Serientimer anlegen"

msgid "Create Switch Timer"
msgstr "Umschalttimer anlegen"

msgid "Delete Switch Timer"
msgstr "Umschalttimer löschen"

msgid "Search"
msgstr "Suchen"

msgid "Check for Timer Conflicts"
msgstr "Auf Timerkonflikte prüfen"

msgid "Search in Recordings"
msgstr "In Aufnahmen suchen"

msgid "Set Folder for"
msgstr "Verzeichnis festlegen für"

msgid "Timer changed"
msgstr "Timer geändert"

msgid "Timer created"
msgstr "Timer angelegt"

msgid "Timer NOT created"
msgstr "Timer NICHT angelegt"

msgid "OK"
msgstr "OK"

msgid "Timer deleted"
msgstr "Timer gelöscht"

msgid "Timer"
msgstr "Timer"

msgid "still recording - really delete?"
msgstr "Aufzeichnung läuft - wirklich löschen?"

msgid "Yes"
msgstr "Ja"

msgid "No"
msgstr "Nein"

msgid "Timer Conflicts"
msgstr "Timerkonflikte"

msgid "detected"
msgstr "gefunden"

msgid "Ignore Conflicts"
msgstr "Konflikte ignorieren"

msgid "Show conflict"
msgstr "Konflikt zeigen"

msgid "timers involved"
msgstr "Timer beteiligt"

msgid "Ignore Conflict"
msgstr "Konflikt ignorieren"

msgid "No Timer Conflicts found"
msgstr "Keine Timerkonflikte gefunden"

msgid "Close"
msgstr "Schließen"

msgid "reruns for"
msgstr "Wiederholungen für"

msgid "rerun for"
msgstr "Wiederholung für"

msgid "found"
msgstr "gefunden"

msgid "Ignore reruns"
msgstr "Wiederholungen ignorieren"

msgid "No reruns found for Event"
msgstr "Keine Wiederholungen gefunden für"

msgid "Timer for"
msgstr "Timer für"

msgid "replaced by rerun"
msgstr "ersetzt durch Wiederholung"

msgid "Save"
msgstr "Speichern"

msgid "Cancel"
msgstr "Abbrechen"

msgid "Timer Active"
msgstr "Timer aktiv"

msgid "Priority"
msgstr "Priorität"

msgid "Lifetime"
msgstr "Lebensdauer"

msgid "Day"
msgstr "Tag"

msgid "Timer start time"
msgstr "Timer Start Zeit"

msgid "Timer stop time"
msgstr "Timer Stop Zeit"

msgid "Timer File"
msgstr "Timer Datei"

msgid "New Folder"
msgstr "Neues Verzeichnis"

msgid "Create Series Timer based on:"
msgstr "Serientimer anlegen basierend auf:"

msgid "Create Timer"
msgstr "Timer anlegen"

msgid "Series Timer start time"
msgstr "Serientimer Start Zeit"

msgid "Series Timer stop time"
msgstr "Serientimer Stop Zeit"

msgid "Days to record"
msgstr "Tage"

msgid "Day to start"
msgstr "Beginnen am"

msgid "Series Timer created"
msgstr "Serientimer angelegt"

msgid "Start"
msgstr "Start"

msgid "Stop"
msgstr "Stop"

msgid "Configure Search Timer based on:"
msgstr "Suchtimer konfigurieren basierend auf:"

msgid "Continue"
msgstr "Weiter"

msgid "Search Expression:"
msgstr "Suchausdruck:"

msgid "Configure Search Timer for Search String:"
msgstr "Suchtimer konfigurieren für Suchbegriff:"

msgid "Manually configure Options"
msgstr "Optionen manuell konfigurieren"

msgid "Use Template"
msgstr "Template benutzen"

msgid "EPGSearch Search Timers"
msgstr "EPGSearch Suchtimer"

msgid "No Search Timers Configured"
msgstr "Keine Suchtimer angelegt"

msgid "Configure Search Timer Options"
msgstr "Suchtimer konfigurieren"

msgid "Save Search Timer"
msgstr "Suchtimer speichern"

msgid "Search term"
msgstr "Suchbegriff"

msgid "Active"
msgstr "Aktiv"

msgid "Search mode"
msgstr "Suchmodus"

msgid "Tolerance"
msgstr "Toleranz"

msgid "Match case"
msgstr "Groß/klein"

msgid "Use title"
msgstr "Verwende Titel"

msgid "Use subtitle"
msgstr "Verwende Untertitel"

msgid "Use description"
msgstr "Verwende Beschreibung"

msgid "Use channel"
msgstr "Verwende Kanal"

msgid "from channel"
msgstr "von Kanal"

msgid "to channel"
msgstr "bis Kanal"

msgid "Channel group"
msgstr "Kanalgruppe"

msgid "Use time"
msgstr "Verwende Uhrzeit"

msgid "Start after"
msgstr "Beginn nach"

msgid "Start before"
msgstr "Beginn vor"

msgid "Use duration"
msgstr "Verwende Dauer"

msgid "Min. duration"
msgstr "Min. Dauer"

msgid "Max. duration"
msgstr "Max. Dauer"

msgid "Use day of week"
msgstr "Verwende Wochentag"

msgid "Day of week"
msgstr "Wochentag"

msgid "Use in Favorites"
msgstr "Als Favorit benutzen"

msgid "Use as search timer"
msgstr "Als Suchtimer verwenden"

msgid "Action"
msgstr "Aktion"

msgid "Switch ... minutes before start"
msgstr "Umschalten ... Minuten vor Start"

msgid "Unmute sound"
msgstr "Ton anschalten"

msgid "Ask ... minutes before start"
msgstr "Nachfrage ... Minuten vor Start"

msgid "Series Recording"
msgstr "Serienaufnahme"

msgid "Directory"
msgstr "Verzeichnis"

msgid "Delete recordings after ... days"
msgstr "Aufn. nach ... Tagen löschen"

msgid "Keep ... recordings"
msgstr "Behalte ... Aufnahmen"

msgid "Pause when ... recordings exist"
msgstr "Pause, wenn ... Aufnahmen exist."

msgid "Avoid Repeats"
msgstr "Vermeide Wiederholung"

msgid "Allowed repeats"
msgstr "Erlaubte Wiederholungen"

msgid "Only repeats within ... days"
msgstr "Nur Wiederh. innerhalb von ... Tagen"

msgid "Compare Title"
msgstr "Vergleiche Titel"

msgid "Compare Subtitle"
msgstr "Vergleiche Untertitel"

msgid "Compare Description"
msgstr "Vergleiche Beschreibung"

msgid "Min. match in %"
msgstr "Min. Übereinstimmung in %"

msgid "Compare date"
msgstr "Vergleiche Zeitpunkt"

msgid "Time margin for start in minutes"
msgstr "Vorlauf zum Timer-Beginn (min)"

msgid "Time margin for stop in minutes"
msgstr "Nachlauf zum Timer-Ende (min)"

msgid "Use VPS"
msgstr "Verwende VPS"

msgid "Auto delete"
msgstr "automatisch löschen"

msgid "after ... recordings"
msgstr "nach ... Aufnahmen"

msgid "after ... days after first rec."
msgstr "nach ... Tagen nach erster Aufn."

msgid "Display Results for Search Timer"
msgstr "Ergebnisse für Suchtimer anzeigen"

msgid "Really delete Search Timer"
msgstr "Suchtimer wirklich löschen"

msgid "Delete only Search Timer"
msgstr "Nur Suchtimer löschen"

msgid "Delete Search Timer and created Timers"
msgstr "Suchtimer und erzeugte Timer löschen"

msgid "Search Timer sucessfully created"
msgstr "Suchtimer erfolgreich angelegt"

msgid "Search Timer update initialised"
msgstr "Suchtimer update initialisiert"

msgid "Search Timer NOT sucessfully created"
msgstr "Suchtimer NICHT erfolgreich angelegt"

msgid "Creating Search Timer"
msgstr "Suchtimer anlegen"

msgid "Search Term"
msgstr "Suchbegriff"

msgid "Using Template"
msgstr "Template"

msgid "Use other Template"
msgstr "Anderes Template benutzen"

msgid "search results for Favorite"
msgstr "Suchergebnisse für Favorit"

msgid "search results for Search Timer"
msgstr "Treffer für Suchtimer"

msgid "search result for Favorite"
msgstr "Suchergebnis für Favorit"

msgid "search result for Search Timer"
msgstr "Treffer für Suchtimer"

msgid "Nothing found for Search String"
msgstr "Keine Treffer für Suchbegriff"

msgid "Configure Options for Switchtimer"
msgstr "Optionen für Umschalttimer konfigurieren"

msgid "Create"
msgstr "Anlegen"

msgid "switch"
msgstr "umschalten"

msgid "announce only"
msgstr "nur ankündigen"

msgid "ask for switch"
msgstr "vor umschalten fragen"

msgid "Minutes before switching"
msgstr "Minuten vor umschalten"

msgid "Switch Mode"
msgstr "Umschaltmodus"

msgid "Switch Timer sucessfully created"
msgstr "Umschalttimer erfolgreich angelegt"

msgid "Switch Timer NOT sucessfully created"
msgstr "Umschalttimer NICHT erfolgreich angelegt"

msgid "Switch Timer deleted"
msgstr "Umschalttimer gelöscht"

msgid "Perform Search"
msgstr "Suche ausführen"

msgid "Search Mode"
msgstr "Suchmodus"

msgid "Channel to Search"
msgstr "Suche auf Kanal"

msgid "Search in title"
msgstr "In Titel suchen"

msgid "Search in Subtitle"
msgstr "In Untertitel suchen"

msgid "Search in Description"
msgstr "In Beschreibung suchen"

msgid "Show Search Options"
msgstr "Suchoptionen anzeigen"

msgid "search results for"
msgstr "Suchergebnisse für"

msgid "search result for"
msgstr "Suchergebnis für"

msgid "Adapt Search"
msgstr "Suche anpassen"

msgid "Search String has to have at least three letters"
msgstr "Suchausdruck muss mindestens drei Zeichen haben"

msgid "Found"
msgstr " "

msgid "recordings"
msgstr "Aufnahmen gefunden"

msgid "recording"
msgstr "Aufnahme gefunden"

msgid "for"
msgstr "für"

msgid "No recordings found for"
msgstr "Keine Aufnahmen gefunden für"

msgid "No Favorites available"
msgstr "Keine Favoriten verfügbar"

msgid "What's on now"
msgstr "Was läuft jetzt?"

msgid "What's on next"
msgstr "Was läuft als nächstes?"

msgid "whole term must appear"
msgstr "vollständiger Ausdruck"

msgid "all terms must exist"
msgstr "alle Worte"

msgid "one term must exist"
msgstr "ein Wort"

msgid "exact match"
msgstr "exakt"

msgid "regular expression"
msgstr "Regulärer Ausdruck"

msgid "fuzzy"
msgstr "unscharf"

msgid "allow empty"
msgstr "erlaube leere"

msgid "Interval"
msgstr "Bereich"

msgid "Channel Group"
msgstr "Kanalgruppe"

msgid "only FTA"
msgstr "ohne PayTV"

msgid "same day"
msgstr "gleicher Tag"

msgid "same week"
msgstr "gleiche Woche"

msgid "same month"
msgstr "gleicher Monat"

msgid "Record"
msgstr "Aufnehmen"

msgid "Announce by OSD"
msgstr "per OSD ankündigen"

msgid "Switch only"
msgstr "Nur umschalten"

msgid "Announce and switch"
msgstr "Ankündigen und Umschalten"

msgid "Announce by mail"
msgstr "per Mail ankündigen"

msgid "Inactive record"
msgstr "inaktive Aufnahme"

msgid "no"
msgstr "Nein"

msgid "count recordings"
msgstr "Anzahl Aufnahmen"

msgid "count days"
msgstr "Anzahl Tage"

msgid "General Settings"
msgstr "Allgemeine Einstellungen"

msgid "Screen Presentation"
msgstr "Anzeigeoptionen"

msgid "Fonts and Fontsizes"
msgstr "Schriften und Schriftgrößen"

msgid "Recording Menus and Favorites"
msgstr "Aufzeichnungsmenü und Favoriten"

msgid "Image Loading and Caching"
msgstr "Image Loading und Caching"

msgid "x channels back / forward"
msgstr "x Kanäle zurück / vor"

msgid "previous / next channel group"
msgstr "vorherige / nächste Kanalgruppe"

msgid "Blue: Channel Switch, Ok: Detailed EPG"
msgstr "Blau: Umschalten, OK: Detailiertes EPG"

msgid "Blue: Detailed EPG, Ok: Channel Switch"
msgstr "Blau: Detailiertes EPG, OK: Umschalten"

msgid "Blue: Favorites / Switch, Ok: Detailed EPG"
msgstr "Blau: Favoriten / Umschalten, OK: Det. EPG"

msgid "Timely Jump"
msgstr "Zeitsprung"

msgid "Jump to specific channel"
msgstr "Sprung zu deinem bestimmten Kanal"

msgid "never"
msgstr "nie"

msgid "if exists"
msgstr "falls vorhanden"

msgid "always"
msgstr "immer"

msgid "Use workaround for HWAccelerated OSD"
msgstr ""

msgid "Show Main Menu Entry"
msgstr "Hauptmenüeintrag anzeigen"

msgid "Replace VDR Schedules Menu"
msgstr "VDR Programm Menü ersetzen"

msgid "Use appropriate nOpacity Theme"
msgstr "Entsprechendes nOpacity Theme benutzen"

msgid "Theme"
msgstr "Theme"

msgid "Time to display in minutes"
msgstr "Angezeigte Zeitspanne in Minuten"

msgid "Rounded Corners"
msgstr "Abgerundete Ecken"

msgid "Channel Jump Mode (Keys Green / Yellow)"
msgstr "Kanalsprung Modus (Tasten grün / gelb)"

msgid "Keys Blue and OK"
msgstr "Tasten Blau und OK"

msgid "Close TVGuide after channel switch"
msgstr "TVGuide nach Umschalten schließen"

msgid "Functionality of numeric Keys"
msgstr "Funktion der Nummerntasten"

msgid "Hide last Channel Group"
msgstr "Letzte Kanalgruppe verstecken"

msgid "Big Step (Keys 1 / 3) in hours"
msgstr "Großer Sprung (Tasten 1 / 3) in Stunden"

msgid "Huge Step (Keys 4 / 6) in hours"
msgstr "Sehr großer Sprung (Tasten 4 / 6) in Stunden"

msgid "Time Format (12h/24h)"
msgstr "Zeitformat (12h/24h)"

msgid "EPG Window Text Scrolling Speed"
msgstr "Text Scroll Geschwindigkeit des EPG Fensters"

msgid "Display Reruns in detailed EPG View"
msgstr "Wiederholungen in der detailierten EPG Ansicht anzeigen"

msgid "Number of reruns to display"
msgstr "Anzahl der dargestellten Wiederholungen"

msgid "Use Subtitle for reruns"
msgstr "Untertitel für Wiederholungssuche nutzen"

msgid "Display Mode"
msgstr "Anzeigemodus"

msgid "Height of Channel Header (Perc. of osd height)"
msgstr "Höhe des Kanalheaders (% der OSD Höhe)"

msgid "Width of Timeline (Perc. of osd width)"
msgstr "Breite der Zeitleiste (% der OSD Breite)"

msgid "Number of Channels to display"
msgstr "Anzahl der angezeigten Kanäle"

msgid "Width of Channel Header (Perc. of osd width)"
msgstr "Breite des Kanalheaders (% der OSD Breite)"

msgid "Height of Timeline (Perc. of osd height)"
msgstr "Höhe der Zeitleiste (% der OSD Höhe)"

msgid "Display time in EPG Grids"
msgstr "Zeit in EPG Grids anzeigen"

msgid "Height of Headers (Status Header and EPG View, Perc. of osd height)"
msgstr "Höhe der Header (Status Header und EPG View, % der OSD Höhe)"

msgid "Height of Footer (Perc. of osd height)"
msgstr "Höhe des Footers (% der OSD Höhe)"

msgid "Display status header"
msgstr "Status Header anzeigen"

msgid "Scale video to upper right corner"
msgstr "Video in obere rechte Ecke skalieren"

msgid "Rounded corners around video frame"
msgstr "Abgerundete Ecken um Videofenster"

msgid "Display Channel Names in Header"
msgstr "Kanalnamen im Header anzeigen"

msgid "Display channel groups"
msgstr "Kanalgruppen anzeigen"

msgid "Height of channel groups (Perc. of osd height)"
msgstr "Höhe der Kanalgruppen (% der OSD Höhe)"

msgid "Width of channel groups (Perc. of osd width)"
msgstr "Breite der Kanalgruppen (% der OSD Breite)"

msgid "Display current time baseline"
msgstr "Linie für aktuelle Uhrzeit anzeigen"

msgid "Show Channel Logos"
msgstr "Kanallogos anzeigen"

msgid "Logo Path used"
msgstr "Benutzer Pfad für Kanallogos"

msgid "Logo Extension"
msgstr "Logo Extension"

msgid "Logo width ratio"
msgstr "Logo Breitenverhältnis"

msgid "Logo height ratio"
msgstr "Logo Höhenverhältnis"

msgid "Text Border in Detailed View (pixel)"
msgstr "Rand im detailierten EPG View"

msgid "Show EPG Images"
msgstr "EPG Bilder anzeigen"

msgid "EPG Images Path used"
msgstr "Benutzer EPG Bilder Pfad"

msgid "EPG Image width"
msgstr "Breite der EPG Bilder"

msgid "EPG Image height"
msgstr "Höhe der EPG Bilder"

msgid "Number of additional EPG Images"
msgstr "Anzahl zusätzlicher EPG Bilder"

msgid "Additional EPG Image width"
msgstr "Breite der zus. EPG Bilder"

msgid "Additional EPG Image height"
msgstr "Höhe der zus. EPG Bilder"

msgid "Font"
msgstr "Schriftart"

msgid "Status Header Font Size"
msgstr "Status Header Schriftgröße"

msgid "Status Header Large Font Size"
msgstr "Status Header große Schriftgröße"

msgid "Detail EPG View Font Size"
msgstr "Detailierte EPG Ansicht Schriftgröße"

msgid "Detail EPG View Header Font Size"
msgstr "Detailierte EPG Ansicht Header Schriftgröße"

msgid "Message Font Size"
msgstr "Nachrichten Schriftgröße"

msgid "Message Large Font Size"
msgstr "Nachrichten große Schriftgröße"

msgid "Button Font Size"
msgstr "Button Schriftgröße"

msgid "Channel Header Font Size"
msgstr "Kanal Header Schriftgröße"

msgid "Channel Groups Font Size"
msgstr "Kanalgruppen Schriftgröße"

msgid "Grid Font Size"
msgstr "Grid Schriftgröße"

msgid "Grid Font Small Size"
msgstr "Grid kleine Schriftgröße"

msgid "Timeline Weekday Font Size"
msgstr "Zeitleiste Wochentag Schriftgröße"

msgid "Timeline Date Font Size"
msgstr "Zeitleiste Datum Schriftgröße"

msgid "Timeline Time Font Size"
msgstr "Zeitleiste Zeit Schriftgröße"

msgid "Search & Recording Menu Font Size"
msgstr "Suchen & Aufnehmen Menu Schriftgröße"

msgid "Search & Recording Menu Small Font Size"
msgstr "Suchen & Aufnehmen Menu kleine Schriftgröße"

msgid "Search & Recording Menu Header Font Size"
msgstr "Suchen & Aufnehmen Menu Header Schriftgröße"

msgid "Always use root video folder"
msgstr "Immer root video Verzeichnis benutzen"

msgid "Select from folder list"
msgstr "Verzeichnis aus Liste auswählen"

msgid "Use fixed folder"
msgstr "Festes Verzeichnis benutzen"

msgid "smart"
msgstr "intelligent"

msgid "Instant recording:"
msgstr "Sofortaufnahmen:"

msgid "Folder for instant Recordings"
msgstr "Verzeichnis für Sofortaufnahmen"

msgid "Folder"
msgstr "Verzeichnis"

msgid "Add episode to manual timers"
msgstr "Untertitel in manuellen Timern"

msgid "Use Remotetimers"
msgstr "RemoteTimers benutzen"

msgid "Show timer confirmation messages"
msgstr "Timer Bestätigungsmeldungen anzeigen"

msgid "Favorites:"
msgstr "Favoriten:"

msgid "Limit channels in favorites"
msgstr "Kanäle in Favoriten beschränken"

msgid "Start Channel"
msgstr "von Kanal"

msgid "Stop Channel"
msgstr "bis Kanal"

msgid "Use \"What's on now\" in favorites"
msgstr "\"Was läuft jetzt\" in Favoriten benutzen"

msgid "Use \"What's on next\" in favorites"
msgstr "\"Was läuft als nächstes\" in Favoriten benutzen"

msgid "User defined times in favorites:"
msgstr "Nutzer definierte Zeiten in Favoriten:"

msgid "Use user defined time 1"
msgstr "Verw. benutzerdef. Zeit 1"

msgid "Description"
msgstr "Beschreibung"

msgid "Time"
msgstr "Zeit"

msgid "Use user defined time 2"
msgstr "Verw. benutzerdef. Zeit 2"

msgid "Use user defined time 3"
msgstr "Verw. benutzerdef. Zeit 3"

msgid "Use user defined time 4"
msgstr "Verw. benutzerdef. Zeit 4"

msgid "Switchtimer:"
msgstr "Umschalttimer:"

msgid "Switch (x)min before start of the show"
msgstr "Umschalten (x)min vor der Sendung"

msgid "Create Log Messages for image loading"
msgstr "Log Nachrichten für das Laden der Bilder erzeugen"

msgid "Limit Logo Cache"
msgstr "Logo Cache beschränken"

msgid "Maximal number of logos to cache"
msgstr "Maximale Anzahl Logos"

msgid "Number of  logos to cache at start"
msgstr "Anzahl der zu cachenden Logos beim Start"

msgid "Cache Sizes"
msgstr "Cache Größe"

msgid "OSD Element Cache"
msgstr "OSD Element Cache"

msgid "Logo cache"
msgstr "Logo Cache"

msgid "EPG Grid Cache"
msgstr "EPG Grid Cache"

msgid "Channel Groups Cache"
msgstr "Kanalgruppen Cache"

msgid "Recording Menus Icon Cache"
msgstr "Recording Menüs Icon Cache"

msgid "A fancy 2d EPG Viewer"
msgstr "Eine schicke Programm Vorschau"

msgid "No Cast available"
msgstr "Keine Besetzung vorhanden"

msgid "Cast"
msgstr "Besetzung"

msgid "EPG Info"
msgstr "EPG Info"

msgid "Reruns"
msgstr "Wiederholungen"

msgid "Recording Information"
msgstr "Aufnahme Information"

msgid "Image Galery"
msgstr "Bildergalerie"

msgid "TheTVDB Info"
msgstr "TheTVDB Info"

msgid "TheTVDB Information"
msgstr "TheTVDB Information"

msgid "Episode"
msgstr "Episode"

msgid "Season"
msgstr "Staffel"

msgid "Episode Overview"
msgstr "Episodenüberblick"

msgid "First aired"
msgstr "Erstausstrahlung"

msgid "Guest Stars"
msgstr "Gast Stars"

msgid "TheMovieDB Rating"
msgstr "TheMovieDB Wertung"

msgid "Series Overview"
msgstr "Serienüberblick"

msgid "Genre"
msgstr "Genre"

msgid "Network"
msgstr "Sendeanstalt"

msgid "Status"
msgstr "Status"

msgid "TheMovieDB Information"
msgstr "TheMovieDB Information"

msgid "Original Title"
msgstr "Original Titel"

msgid "Tagline"
msgstr "Zusammenfassung"

msgid "Overview"
msgstr "Überblick"

msgid "yes"
msgstr "Ja"

msgid "Adult"
msgstr "Nur für Erwachsene"

msgid "Collection"
msgstr "Kollektion"

msgid "Budget"
msgstr "Budget"

msgid "Revenue"
msgstr "Einnahmen"

msgid "Homepage"
msgstr "Homepage"

msgid "Release Date"
msgstr "Veröffentlicht"

msgid "Runtime"
msgstr "Laufzeit"

msgid "minutes"
msgstr "Minuten"

msgid "TheMovieDB Popularity"
msgstr "TheMovieDB Popularität"

msgid "TheMovieDB Vote Average"
msgstr "TheMovieDB durchschnittliche Bewertung"

#~ msgid "Display advanced Options"
#~ msgstr "Erweiterte Optionen anzeigen"

#~ msgid "Hide advanced Options"
#~ msgstr "Erweiterte Optionen ausblenden"
