#ifndef __TVGUIDE_TIMELINE_H
#define __TVGUIDE_TIMELINE_H

#include "timemanager.h"
#include "styledpixmap.h"

// --- cTimeLine  -------------------------------------------------------------

class cTimeLine  {
private:
    cTimeManager *timeManager;
    cStyledPixmap *dateViewer;
    cPixmap *timeline;
    cStyledPixmap *clock;
    cPixmap *timeBase;
    cString lastClock;
    void DecorateTile(int posX, int posY, int tileWidth, int tileHeight);
    void DrawRoundedCorners(int posX, int posY, int width, int height, int radius);
    cImage *CreateBackgroundImage(int width, int height, tColor clrBgr, tColor clrBlend);
public:
    cTimeLine(cTimeManager *timeManager);
    virtual ~cTimeLine(void);
    void DrawDateViewer(void);
    void DrawTimeline(void);
    void DrawTimeIndicator(void);
    bool DrawClock(void);
};

#endif //__TVGUIDE_TIMELINE_H
