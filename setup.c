#include "setup.h"

cTvguideSetup::cTvguideSetup() {
    tmpConfig = config;
    Setup();
}

cTvguideSetup::~cTvguideSetup() {
    geoManager.SetGeometry(cOsd::OsdWidth(), cOsd::OsdHeight(), true);
    fontManager.DeleteFonts();
    fontManager.SetFonts();
    config.LoadTheme();
    config.setDynamicValues();
    imgCache.Clear();
    imgCache.CreateCache();
}


void cTvguideSetup::Setup(void) {
    int currentItem = Current();
    Clear();
    
    Add(new cOsdItem(tr("General Settings")));
    Add(new cOsdItem(tr("Screen Presentation")));
    Add(new cOsdItem(tr("Fonts and Fontsizes")));
    Add(new cOsdItem(tr("Recording Menus and Favorites")));
    Add(new cOsdItem(tr("Image Loading and Caching")));

    SetCurrent(Get(currentItem));
    Display();
}

eOSState cTvguideSetup::ProcessKey(eKeys Key) {
    bool hadSubMenu = HasSubMenu();   
    eOSState state = cMenuSetupPage::ProcessKey(Key);
    if (hadSubMenu && Key == kOk)
        Store();
        
    if (!hadSubMenu && (state == osUnknown || Key == kOk)) {
        if ((Key == kOk && !hadSubMenu)) {
            const char* ItemText = Get(Current())->Text();
            if (strcmp(ItemText, tr("General Settings")) == 0)
                state = AddSubMenu(new cMenuSetupGeneral(&tmpConfig));
            if (strcmp(ItemText, tr("Screen Presentation")) == 0)
                state = AddSubMenu(new cMenuSetupScreenLayout(&tmpConfig));
            if (strcmp(ItemText, tr("Fonts and Fontsizes")) == 0)
                state = AddSubMenu(new cMenuSetupFont(&tmpConfig));
            if (strcmp(ItemText, tr("Recording Menus and Favorites")) == 0)
                state = AddSubMenu(new cMenuSetupFavorites(&tmpConfig));
            if (strcmp(ItemText, tr("Image Loading and Caching")) == 0)
                state = AddSubMenu(new cMenuSetupImageCache(&tmpConfig));  
        }
    }   
    return state;
}

void cTvguideSetup::Store(void) {

    config = tmpConfig;
    SetupStore("debugImageLoading", config.debugImageLoading);
    SetupStore("useNopacityTheme", config.useNopacityTheme);
    SetupStore("themeIndex", config.themeIndex);
    SetupStore("useHWAccel", config.useHWAccel);
    SetupStore("showMainMenuEntry", config.showMainMenuEntry);
    SetupStore("replaceOriginalSchedule", config.replaceOriginalSchedule);
    SetupStore("displayMode", config.displayMode);
    SetupStore("showTimeInGrid", config.showTimeInGrid);
    SetupStore("displayStatusHeader", config.displayStatusHeader);
    SetupStore("displayChannelGroups", config.displayChannelGroups);
    SetupStore("displayTimeBase", config.displayTimeBase);
    SetupStore("headerHeightPercent", config.headerHeightPercent);
    SetupStore("channelGroupsPercent", config.channelGroupsPercent);
    SetupStore("epgViewBorder", config.epgViewBorder);
    SetupStore("scaleVideo", config.scaleVideo);
    SetupStore("decorateVideo", config.decorateVideo);
    SetupStore("roundedCorners", config.roundedCorners);
    SetupStore("timeFormat", config.timeFormat);
    SetupStore("channelCols", config.channelCols);
    SetupStore("channelRows", config.channelRows);
    SetupStore("displayTime", config.displayTime);
    SetupStore("displayHorizontalTime", config.displayHorizontalTime);
    SetupStore("bigStepHours", config.bigStepHours);
    SetupStore("bigStepHoursHorizontal", config.bigStepHoursHorizontal);
    SetupStore("hugeStepHours", config.hugeStepHours);
    SetupStore("hugeStepHoursHorizontal", config.hugeStepHoursHorizontal);
    SetupStore("channelJumpMode", config.channelJumpMode);
    SetupStore("blueKeyMode", config.blueKeyMode);
    SetupStore("numkeyMode", config.numkeyMode);
    SetupStore("useRemoteTimers", config.useRemoteTimers);
    SetupStore("closeOnSwitch", config.closeOnSwitch);
    SetupStore("hideLastGroup", config.hideLastGroup);
    SetupStore("hideChannelLogos", config.hideChannelLogos);
    SetupStore("logoExtension", config.logoExtension);
    SetupStore("logoWidthRatio", config.logoWidthRatio);
    SetupStore("logoHeightRatio", config.logoHeightRatio);
    SetupStore("hideEpgImages", config.hideEpgImages);
    SetupStore("epgImageWidth", config.epgImageWidth);
    SetupStore("epgImageHeight", config.epgImageHeight);
    SetupStore("numAdditionalEPGPictures", config.numAdditionalEPGPictures);
    SetupStore("epgImageWidthLarge", config.epgImageWidthLarge);
    SetupStore("epgImageHeightLarge", config.epgImageHeightLarge);
    SetupStore("detailedViewScrollStep", config.detailedViewScrollStep);
    SetupStore("timeLineWidthPercent", config.timeLineWidthPercent);
    SetupStore("timeLineHeightPercent", config.timeLineHeightPercent);
    SetupStore("displayChannelName", config.displayChannelName);
    SetupStore("channelHeaderWidthPercent", config.channelHeaderWidthPercent);
    SetupStore("channelHeaderHeightPercent", config.channelHeaderHeightPercent);
    SetupStore("footerHeightPercent", config.footerHeightPercent);
    SetupStore("instRecFolderMode", config.instRecFolderMode);
    SetupStore("instRecFixedFolder", config.instRecFixedFolder.c_str());
    SetupStore("addSubtitleToTimer", config.addSubtitleToTimer);
    SetupStore("timerMessage", config.timerMessage);
    SetupStore("favWhatsOnNow", config.favWhatsOnNow);
    SetupStore("favWhatsOnNext", config.favWhatsOnNext);
    SetupStore("favUseTime1", config.favUseTime1);
    SetupStore("favUseTime2", config.favUseTime2);
    SetupStore("favUseTime3", config.favUseTime3);
    SetupStore("favUseTime4", config.favUseTime4);
    SetupStore("favTime1", config.favTime1);
    SetupStore("favTime2", config.favTime2);
    SetupStore("favTime3", config.favTime3);
    SetupStore("favTime4", config.favTime4);
    SetupStore("descUser1", config.descUser1.c_str());
    SetupStore("descUser2", config.descUser2.c_str());
    SetupStore("descUser3", config.descUser3.c_str());
    SetupStore("descUser4", config.descUser4.c_str());
    SetupStore("favLimitChannels", config.favLimitChannels);
    SetupStore("favStartChannel", config.favStartChannel);
    SetupStore("favStopChannel", config.favStopChannel);
    SetupStore("switchMode", config.switchMode);
    SetupStore("switchMinsBefore", config.switchMinsBefore);
    SetupStore("fontIndex", config.fontIndex);
    SetupStore("FontButtonDelta", config.FontButtonDelta);
    SetupStore("FontDetailViewDelta", config.FontDetailViewDelta);
    SetupStore("FontDetailHeaderDelta", config.FontDetailHeaderDelta);
    SetupStore("FontMessageBoxDelta", config.FontMessageBoxDelta);
    SetupStore("FontMessageBoxLargeDelta", config.FontMessageBoxLargeDelta);
    SetupStore("FontStatusHeaderDelta", config.FontStatusHeaderDelta);
    SetupStore("FontStatusHeaderLargeDelta", config.FontStatusHeaderLargeDelta);
    SetupStore("FontChannelHeaderDelta", config.FontChannelHeaderDelta);
    SetupStore("FontChannelGroupsDelta", config.FontChannelGroupsDelta);
    SetupStore("FontGridDelta", config.FontGridDelta);
    SetupStore("FontGridSmallDelta", config.FontGridSmallDelta);
    SetupStore("FontTimeLineWeekdayDelta", config.FontTimeLineWeekdayDelta);
    SetupStore("FontTimeLineDateDelta", config.FontTimeLineDateDelta);
    SetupStore("FontTimeLineTimeDelta", config.FontTimeLineTimeDelta);
    SetupStore("FontChannelHeaderHorizontalDelta", config.FontChannelHeaderHorizontalDelta);
    SetupStore("FontChannelGroupsHorizontalDelta", config.FontChannelGroupsHorizontalDelta);
    SetupStore("FontGridHorizontalDelta", config.FontGridHorizontalDelta);
    SetupStore("FontGridHorizontalSmallDelta", config.FontGridHorizontalSmallDelta);
    SetupStore("FontTimeLineDateHorizontalDelta", config.FontTimeLineDateHorizontalDelta);
    SetupStore("FontTimeLineTimeHorizontalDelta", config.FontTimeLineTimeHorizontalDelta);
    SetupStore("FontRecMenuItemDelta", config.FontRecMenuItemDelta);
    SetupStore("FontRecMenuItemSmallDelta", config.FontRecMenuItemSmallDelta);
    SetupStore("FontRecMenuItemLargeDelta", config.FontRecMenuItemLargeDelta);
    SetupStore("displayRerunsDetailEPGView", config.displayRerunsDetailEPGView);
    SetupStore("numReruns", config.numReruns);
    SetupStore("useSubtitleRerun", config.useSubtitleRerun);
    SetupStore("numLogosInitial", config.numLogosInitial);
    SetupStore("numLogosMax", config.numLogosMax);
    SetupStore("limitLogoCache", config.limitLogoCache);
}

cMenuSetupSubMenu::cMenuSetupSubMenu(const char* Title, cTVGuideConfig* data) : cOsdMenu(Title, 40) {
    tmpConfig = data;
    indent = "    ";
}

cOsdItem *cMenuSetupSubMenu::InfoItem(const char *label, const char *value) {
    cOsdItem *item;
    item = new cOsdItem(cString::sprintf("%s: %s", label, value));
    item->SetSelectable(false);
    return item;
}

eOSState cMenuSetupSubMenu::ProcessKey(eKeys Key) {
  eOSState state = cOsdMenu::ProcessKey(Key);
  if (state == osUnknown) {
    switch (Key) {
      case kOk:
        return osBack;
      default:
        break;
    }
  }
  return state;
}

//----- General Settings -------------------------------------------------------------------------------------------------------------

cMenuSetupGeneral::cMenuSetupGeneral(cTVGuideConfig* data)  : cMenuSetupSubMenu(tr("General Settings"), data) {
    themes.Load(*cString("tvguide"));
    timeFormatItems[0] = "12h";
    timeFormatItems[1] = "24h";
    jumpMode[0] = tr("x channels back / forward");
    jumpMode[1] = tr("previous / next channel group");
    blueMode[0] = tr("Blue: Channel Switch, Ok: Detailed EPG");
    blueMode[1] = tr("Blue: Detailed EPG, Ok: Channel Switch");
    blueMode[2] = tr("Blue: Favorites / Switch, Ok: Detailed EPG");
    numMode[0] = tr("Timely Jump");
    numMode[1] = tr("Jump to specific channel");
    useSubtitleRerunTexts[0] = tr("never");
    useSubtitleRerunTexts[1] = tr("if exists");
    useSubtitleRerunTexts[2] = tr("always");
    Set();
}

void cMenuSetupGeneral::Set(void) {
    int currentItem = Current();
    Clear();
    Add(new cMenuEditBoolItem(tr("Use workaround for HWAccelerated OSD"), &tmpConfig->useHWAccel));
    Add(new cMenuEditBoolItem(tr("Show Main Menu Entry"), &tmpConfig->showMainMenuEntry));
    Add(new cMenuEditBoolItem(tr("Replace VDR Schedules Menu"), &tmpConfig->replaceOriginalSchedule));
    Add(new cMenuEditBoolItem(tr("Use appropriate nOpacity Theme"), &tmpConfig->useNopacityTheme));
    if (!tmpConfig->useNopacityTheme) {
        if (themes.NumThemes())
            Add(new cMenuEditStraItem(cString::sprintf("%s%s", *indent, tr("Theme")), &tmpConfig->themeIndex, themes.NumThemes(), themes.Descriptions()));
    }
    if (tmpConfig->displayMode == eVertical) {
        Add(new cMenuEditIntItem(tr("Time to display in minutes"), &tmpConfig->displayTime, 60, 320));
    } else if (tmpConfig->displayMode == eHorizontal) {
        Add(new cMenuEditIntItem(tr("Time to display in minutes"), &tmpConfig->displayHorizontalTime, 60, 320));
    }
    Add(new cMenuEditBoolItem(tr("Rounded Corners"), &tmpConfig->roundedCorners));
    
    Add(new cMenuEditStraItem(tr("Channel Jump Mode (Keys Green / Yellow)"), &tmpConfig->channelJumpMode, 2,  jumpMode));
    Add(new cMenuEditStraItem(tr("Keys Blue and OK"), &tmpConfig->blueKeyMode, 3,  blueMode));
    Add(new cMenuEditBoolItem(tr("Close TVGuide after channel switch"), &tmpConfig->closeOnSwitch));
    Add(new cMenuEditStraItem(tr("Functionality of numeric Keys"), &tmpConfig->numkeyMode, 2,  numMode));
    Add(new cMenuEditBoolItem(tr("Hide last Channel Group"), &tmpConfig->hideLastGroup));
    if (tmpConfig->displayMode == eVertical) {
        Add(new cMenuEditIntItem(tr("Big Step (Keys 1 / 3) in hours"), &tmpConfig->bigStepHours, 1, 12));
        Add(new cMenuEditIntItem(tr("Huge Step (Keys 4 / 6) in hours"), &tmpConfig->hugeStepHours, 13, 48));
    } else if (tmpConfig->displayMode == eHorizontal) {
        Add(new cMenuEditIntItem(tr("Big Step (Keys 1 / 3) in hours"), &tmpConfig->bigStepHoursHorizontal, 1, 12));
        Add(new cMenuEditIntItem(tr("Huge Step (Keys 4 / 6) in hours"), &tmpConfig->hugeStepHoursHorizontal, 13, 48));
    }
    Add(new cMenuEditStraItem(tr("Time Format (12h/24h)"), &tmpConfig->timeFormat, 2,  timeFormatItems));
    Add(new cMenuEditIntItem(tr("EPG Window Text Scrolling Speed"), &tmpConfig->detailedViewScrollStep, 1, 30));
    Add(new cMenuEditBoolItem(tr("Display Reruns in detailed EPG View"), &tmpConfig->displayRerunsDetailEPGView));
    if (tmpConfig->displayRerunsDetailEPGView) {
        Add(new cMenuEditIntItem(cString::sprintf("%s%s", *indent, tr("Number of reruns to display")), &tmpConfig->numReruns, 1, 10));
        Add(new cMenuEditStraItem(cString::sprintf("%s%s", *indent, tr("Use Subtitle for reruns")), &tmpConfig->useSubtitleRerun, 3, useSubtitleRerunTexts));
    }
    SetCurrent(Get(currentItem));
    Display();
}

eOSState cMenuSetupGeneral::ProcessKey(eKeys Key) {
    bool olduseNopacityTheme = tmpConfig->useNopacityTheme;
    bool olddisplayRerunsDetailEPGView = tmpConfig->displayRerunsDetailEPGView;
    eOSState state = cOsdMenu::ProcessKey(Key);
    if (Key == kOk) {
        state = osBack;
    } else if (Key != kNone) {
        if (tmpConfig->useNopacityTheme != olduseNopacityTheme ||
	    tmpConfig->displayRerunsDetailEPGView != olddisplayRerunsDetailEPGView) {
            Set();
        }
    }
    return state;
}

//----- Screen Presentation -------------------------------------------------------------------------------------------------------------

cMenuSetupScreenLayout::cMenuSetupScreenLayout(cTVGuideConfig* data)  : cMenuSetupSubMenu(tr("Screen Presentation"), data) {
    displayModeItems[0] = "vertical";
    displayModeItems[1] = "horizontal";
    hideChannelLogosItems[0] = trVDR("yes");
    hideChannelLogosItems[1] = trVDR("no");
    logoExtensionItems[0] = "png";
    logoExtensionItems[1] = "jpg";
    Set();
}

void cMenuSetupScreenLayout::Set(void) {
    int currentItem = Current();
    Clear();
    
    Add(new cMenuEditStraItem(tr("Display Mode"), &tmpConfig->displayMode, 2,  displayModeItems));
    if (tmpConfig->displayMode == eVertical) {
        Add(new cMenuEditIntItem(*cString::sprintf("%s%s", *indent, tr("Height of Channel Header (Perc. of osd height)")), &tmpConfig->channelHeaderHeightPercent, 5, 30));
        Add(new cMenuEditIntItem(*cString::sprintf("%s%s", *indent, tr("Width of Timeline (Perc. of osd width)")), &tmpConfig->timeLineWidthPercent, 5, 30));
        Add(new cMenuEditIntItem(*cString::sprintf("%s%s", *indent, tr("Number of Channels to display")), &tmpConfig->channelCols, 3, 12));
    } else if (tmpConfig->displayMode == eHorizontal) {
        Add(new cMenuEditIntItem(*cString::sprintf("%s%s", *indent, tr("Width of Channel Header (Perc. of osd width)")), &tmpConfig->channelHeaderWidthPercent, 5, 30));
        Add(new cMenuEditIntItem(*cString::sprintf("%s%s", *indent, tr("Height of Timeline (Perc. of osd height)")), &tmpConfig->timeLineHeightPercent, 5, 30));
        Add(new cMenuEditIntItem(*cString::sprintf("%s%s", *indent, tr("Number of Channels to display")), &tmpConfig->channelRows, 3, 12));
    }
    Add(new cMenuEditBoolItem(tr("Display time in EPG Grids"), &tmpConfig->showTimeInGrid));
    Add(new cMenuEditIntItem(tr("Height of Headers (Status Header and EPG View, Perc. of osd height)"), &tmpConfig->headerHeightPercent, 10, 50));
    Add(new cMenuEditIntItem(tr("Height of Footer (Perc. of osd height)"), &tmpConfig->footerHeightPercent, 3, 20));
    
    Add(new cMenuEditBoolItem(tr("Display status header"), &tmpConfig->displayStatusHeader));
    if (tmpConfig->displayStatusHeader) {
        Add(new cMenuEditBoolItem(*cString::sprintf("%s%s", *indent, tr("Scale video to upper right corner")), &tmpConfig->scaleVideo));
        Add(new cMenuEditBoolItem(*cString::sprintf("%s%s", *indent, tr("Rounded corners around video frame")), &tmpConfig->decorateVideo));
    }
    
    Add(new cMenuEditBoolItem(tr("Display Channel Names in Header"), &tmpConfig->displayChannelName));
    Add(new cMenuEditBoolItem(tr("Display channel groups"), &tmpConfig->displayChannelGroups));
    if (tmpConfig->displayChannelGroups) {
        if (tmpConfig->displayMode == eVertical) {
            Add(new cMenuEditIntItem(*cString::sprintf("%s%s", *indent, tr("Height of channel groups (Perc. of osd height)")), &tmpConfig->channelGroupsPercent, 3, 30));
        } else if (tmpConfig->displayMode == eHorizontal) {
            Add(new cMenuEditIntItem(*cString::sprintf("%s%s", *indent, tr("Width of channel groups (Perc. of osd width)")), &tmpConfig->channelGroupsPercent, 3, 30));
        }
    }
    
    Add(new cMenuEditBoolItem(tr("Display current time baseline"), &tmpConfig->displayTimeBase));
    Add(new cMenuEditStraItem(tr("Show Channel Logos"), &tmpConfig->hideChannelLogos, 2,  hideChannelLogosItems));   
    if (!tmpConfig->hideChannelLogos) {
        Add(InfoItem(tr("Logo Path used"), *config.logoPath));
        Add(new cMenuEditStraItem(*cString::sprintf("%s%s", *indent, tr("Logo Extension")), &tmpConfig->logoExtension, 2,  logoExtensionItems));
        Add(new cMenuEditIntItem(*cString::sprintf("%s%s", *indent, tr("Logo width ratio")), &tmpConfig->logoWidthRatio, 1, 1000));
        Add(new cMenuEditIntItem(*cString::sprintf("%s%s", *indent, tr("Logo height ratio")), &tmpConfig->logoHeightRatio, 1, 1000));
    }
    
    Add(new cMenuEditIntItem(tr("Text Border in Detailed View (pixel)"), &tmpConfig->epgViewBorder, 0, 300));
    
    Add(new cMenuEditStraItem(tr("Show EPG Images"), &tmpConfig->hideEpgImages, 2,  hideChannelLogosItems)); 
    if (!tmpConfig->hideEpgImages) {
        Add(InfoItem(tr("EPG Images Path used"), *config.epgImagePath));
        Add(new cMenuEditIntItem(*cString::sprintf("%s%s", *indent, tr("EPG Image width")), &tmpConfig->epgImageWidth, 0, 800));
        Add(new cMenuEditIntItem(*cString::sprintf("%s%s", *indent, tr("EPG Image height")), &tmpConfig->epgImageHeight, 0, 800));
        Add(new cMenuEditIntItem(*cString::sprintf("%s%s", *indent, tr("Number of additional EPG Images")), &tmpConfig->numAdditionalEPGPictures, 0, 20));
        Add(new cMenuEditIntItem(*cString::sprintf("%s%s", *indent, tr("Additional EPG Image width")), &tmpConfig->epgImageWidthLarge, 1, 800));
        Add(new cMenuEditIntItem(*cString::sprintf("%s%s", *indent, tr("Additional EPG Image height")), &tmpConfig->epgImageHeightLarge, 0, 800));
    }
    
    SetCurrent(Get(currentItem));
    Display();
}

eOSState cMenuSetupScreenLayout::ProcessKey(eKeys Key) {
    int olddisplayMode = tmpConfig->displayMode;
    bool olddisplayStatusHeader = tmpConfig->displayStatusHeader;
    bool olddisplayChannelGroups = tmpConfig->displayChannelGroups;
    int oldhideChannelLogos = tmpConfig->hideChannelLogos;
    int oldhideEpgImages = tmpConfig->hideEpgImages;
    eOSState state = cOsdMenu::ProcessKey(Key);
    if (Key == kOk) {
        state = osBack;
    } else if (Key != kNone) {
        if (tmpConfig->displayMode != olddisplayMode ||
            tmpConfig->displayStatusHeader != olddisplayStatusHeader ||
            tmpConfig->displayChannelGroups != olddisplayChannelGroups ||
            tmpConfig->hideChannelLogos != oldhideChannelLogos ||
            tmpConfig->hideEpgImages != oldhideEpgImages) {
            Set();
        }
    }
    return state;
}

//-----Fonts and Fontsizes -------------------------------------------------------------------------------------------------------

cMenuSetupFont::cMenuSetupFont(cTVGuideConfig* data)  : cMenuSetupSubMenu(tr("Fonts and Fontsizes"), data) {
    cFont::GetAvailableFontNames(&fontNames);
    fontNames.Insert(strdup(config.fontNameDefault));
    Set();
}

void cMenuSetupFont::Set(void) {
    int currentItem = Current();
    Clear();

    Add(new cMenuEditStraItem(tr("Font"), &tmpConfig->fontIndex, fontNames.Size(), &fontNames[0]));
    
    Add(new cMenuEditIntItem(tr("Status Header Font Size"), &tmpConfig->FontStatusHeaderDelta, -30, 30));    
    Add(new cMenuEditIntItem(tr("Status Header Large Font Size"), &tmpConfig->FontStatusHeaderLargeDelta, -30, 30));
    Add(new cMenuEditIntItem(tr("Detail EPG View Font Size"), &tmpConfig->FontDetailViewDelta, -30, 30));
    Add(new cMenuEditIntItem(tr("Detail EPG View Header Font Size"), &tmpConfig->FontDetailHeaderDelta, -30, 30));
    Add(new cMenuEditIntItem(tr("Message Font Size"), &tmpConfig->FontMessageBoxDelta, -30, 30));
    Add(new cMenuEditIntItem(tr("Message Large Font Size"), &tmpConfig->FontMessageBoxLargeDelta, -30, 30));
    Add(new cMenuEditIntItem(tr("Button Font Size"), &tmpConfig->FontButtonDelta, -30, 30));


    if (tmpConfig->displayMode == eVertical) {
        Add(new cMenuEditIntItem(tr("Channel Header Font Size"), &tmpConfig->FontChannelHeaderDelta, -30, 30));
        Add(new cMenuEditIntItem(tr("Channel Groups Font Size"), &tmpConfig->FontChannelGroupsDelta, -30, 30));
        Add(new cMenuEditIntItem(tr("Grid Font Size"), &tmpConfig->FontGridDelta, -30, 30));
        Add(new cMenuEditIntItem(tr("Grid Font Small Size"), &tmpConfig->FontGridSmallDelta, -30, 30));
        Add(new cMenuEditIntItem(tr("Timeline Weekday Font Size"), &tmpConfig->FontTimeLineWeekdayDelta, -30, 30));
        Add(new cMenuEditIntItem(tr("Timeline Date Font Size"), &tmpConfig->FontTimeLineDateDelta, -30, 30));
        Add(new cMenuEditIntItem(tr("Timeline Time Font Size"), &tmpConfig->FontTimeLineTimeDelta, -30, 30));
    } else if (tmpConfig->displayMode == eHorizontal) {
        Add(new cMenuEditIntItem(tr("Channel Header Font Size"), &tmpConfig->FontChannelHeaderHorizontalDelta, -30, 30));
        Add(new cMenuEditIntItem(tr("Channel Groups Font Size"), &tmpConfig->FontChannelGroupsHorizontalDelta, -30, 30));
        Add(new cMenuEditIntItem(tr("Grid Font Size"), &tmpConfig->FontGridHorizontalDelta, -30, 30));
        Add(new cMenuEditIntItem(tr("Grid Font Small Size"), &tmpConfig->FontGridHorizontalSmallDelta, -30, 30));
        Add(new cMenuEditIntItem(tr("Timeline Date Font Size"), &tmpConfig->FontTimeLineDateHorizontalDelta, -30, 30));
        Add(new cMenuEditIntItem(tr("Timeline Time Font Size"), &tmpConfig->FontTimeLineTimeHorizontalDelta, -30, 30));
    }
    
    Add(new cMenuEditIntItem(tr("Search & Recording Menu Font Size"), &tmpConfig->FontRecMenuItemDelta, -30, 30));
    Add(new cMenuEditIntItem(tr("Search & Recording Menu Small Font Size"), &tmpConfig->FontRecMenuItemSmallDelta, -30, 30));
    Add(new cMenuEditIntItem(tr("Search & Recording Menu Header Font Size"), &tmpConfig->FontRecMenuItemLargeDelta, -30, 30));

    SetCurrent(Get(currentItem));
    Display();
}

//----- Recording Menus and Favorites -------------------------------------------------------------------------------------------------------

cMenuSetupFavorites::cMenuSetupFavorites(cTVGuideConfig* data)  : cMenuSetupSubMenu(tr("Recording Menus and Favorites"), data) {
    strn0cpy(description1, data->descUser1.c_str(), sizeof(description1));
    strn0cpy(description2, data->descUser2.c_str(), sizeof(description2));
    strn0cpy(description3, data->descUser3.c_str(), sizeof(description3));
    strn0cpy(description4, data->descUser4.c_str(), sizeof(description4));
    recFolderMode[0] = tr("Always use root video folder");
    recFolderMode[1] = tr("Select from folder list");
    recFolderMode[2] = tr("Use fixed folder");
    strn0cpy(fixedFolder, data->instRecFixedFolder.c_str(), sizeof(fixedFolder));
    addSubtitleMode[0] = tr("never");
    addSubtitleMode[1] = tr("smart");
    addSubtitleMode[2] = tr("always");
    switchModeItems[0] = (tr("switch"));
    switchModeItems[1] = (tr("announce only"));
    switchModeItems[2] = (tr("ask for switch"));
    Set();
}

void cMenuSetupFavorites::Set(void) {
    int currentItem = Current();
    Clear();

    Add(new cOsdItem(tr("Instant recording:"), osUnknown, false));
    Add(new cMenuEditStraItem(tr("Folder for instant Recordings"), &tmpConfig->instRecFolderMode, 3, recFolderMode));
    if (tmpConfig->instRecFolderMode == eFolderFixed) {
        Add(new cMenuEditStrItem(cString::sprintf("%s%s", *indent, tr("Folder")), fixedFolder, sizeof(fixedFolder), trVDR(FileNameChars)));
    }
    Add(new cMenuEditStraItem(tr("Add episode to manual timers"), &tmpConfig->addSubtitleToTimer, 3, addSubtitleMode));
    if (pRemoteTimers)
        Add(new cMenuEditBoolItem(tr("Use Remotetimers"), &tmpConfig->useRemoteTimers));
    Add(new cMenuEditBoolItem(tr("Show timer confirmation messages"), &tmpConfig->timerMessage));
    Add(new cOsdItem(tr("Favorites:"), osUnknown, false));
    Add(new cMenuEditBoolItem(tr("Limit channels in favorites"), &tmpConfig->favLimitChannels));
    if (tmpConfig->favLimitChannels) {
        Add(new cMenuEditChanItem(tr("Start Channel"), &tmpConfig->favStartChannel));
        Add(new cMenuEditChanItem(tr("Stop Channel"), &tmpConfig->favStopChannel));
    }
    Add(new cMenuEditBoolItem(tr("Use \"What's on now\" in favorites"), &tmpConfig->favWhatsOnNow));
    Add(new cMenuEditBoolItem(tr("Use \"What's on next\" in favorites"), &tmpConfig->favWhatsOnNext));
    Add(new cOsdItem(tr("User defined times in favorites:"), osUnknown, false));
    Add(new cMenuEditBoolItem(tr("Use user defined time 1"), &tmpConfig->favUseTime1));
    if (tmpConfig->favUseTime1) {
        Add(new cMenuEditStrItem(cString::sprintf("%s%s", *indent, tr("Description")), description1, sizeof(description1), trVDR(FileNameChars)));
        Add(new cMenuEditTimeItem(cString::sprintf("%s%s", *indent, tr("Time")), &tmpConfig->favTime1));
    }
    Add(new cMenuEditBoolItem(tr("Use user defined time 2"), &tmpConfig->favUseTime2));
    if (tmpConfig->favUseTime2) {
        Add(new cMenuEditStrItem(cString::sprintf("%s%s", *indent, tr("Description")), description2, sizeof(description2), trVDR(FileNameChars)));
        Add(new cMenuEditTimeItem(cString::sprintf("%s%s", *indent, tr("Time")), &tmpConfig->favTime2));
    }
    Add(new cMenuEditBoolItem(tr("Use user defined time 3"), &tmpConfig->favUseTime3));
    if (tmpConfig->favUseTime3) {
        Add(new cMenuEditStrItem(cString::sprintf("%s%s", *indent, tr("Description")), description3, sizeof(description3), trVDR(FileNameChars)));
        Add(new cMenuEditTimeItem(cString::sprintf("%s%s", *indent, tr("Time")), &tmpConfig->favTime3));
    }
    Add(new cMenuEditBoolItem(tr("Use user defined time 4"), &tmpConfig->favUseTime4));
    if (tmpConfig->favUseTime4) {
        Add(new cMenuEditStrItem(cString::sprintf("%s%s", *indent, tr("Description")), description4, sizeof(description4), trVDR(FileNameChars)));
        Add(new cMenuEditTimeItem(cString::sprintf("%s%s", *indent, tr("Time")), &tmpConfig->favTime4));
    }
    Add(new cOsdItem(tr("Switchtimer:"), osUnknown, false));
    Add(new cMenuEditStraItem(tr("Switch Mode"), &tmpConfig->switchMode, 3,  switchModeItems));
    Add(new cMenuEditIntItem(tr("Switch (x)min before start of the show"), &tmpConfig->switchMinsBefore, 0, 10));


    SetCurrent(Get(currentItem));
    Display();
}

eOSState cMenuSetupFavorites::ProcessKey(eKeys Key) {

    int tmpFavUseTime1 = tmpConfig->favUseTime1;
    int tmpFavUseTime2 = tmpConfig->favUseTime2;
    int tmpFavUseTime3 = tmpConfig->favUseTime3;
    int tmpFavUseTime4 = tmpConfig->favUseTime4;
    int tmpFavLimitChannels = tmpConfig->favLimitChannels;
    int tmpFolderMode = tmpConfig->instRecFolderMode;

    eOSState state = cOsdMenu::ProcessKey(Key);

    if ((tmpFavUseTime1 != tmpConfig->favUseTime1) ||
        (tmpFavUseTime2 != tmpConfig->favUseTime2) ||
        (tmpFavUseTime3 != tmpConfig->favUseTime3) ||
        (tmpFavUseTime4 != tmpConfig->favUseTime4) ||
        (tmpFavLimitChannels != tmpConfig->favLimitChannels) ||
        (tmpFolderMode != tmpConfig->instRecFolderMode) ) {
        Set();
        Display();
    }

    if (state == osUnknown) {
        switch (Key) {
        case kOk: {
            tmpConfig->descUser1 = cString::sprintf("%s", description1);
            tmpConfig->descUser2 = cString::sprintf("%s", description2);
            tmpConfig->descUser3 = cString::sprintf("%s", description3);
            tmpConfig->descUser4 = cString::sprintf("%s", description4);
            return osBack; }
        default:
            break;
        }
    }

    return state;
}

//-----Image Caching-------------------------------------------------------------------------------------------------------------
cMenuSetupImageCache::cMenuSetupImageCache(cTVGuideConfig* data)  : cMenuSetupSubMenu(tr("Image Loading and Caching"), data) {
    Set();
}

void cMenuSetupImageCache::Set(void) {
    int currentItem = Current();
    Clear();
    Add(new cMenuEditBoolItem(tr("Create Log Messages for image loading"), &tmpConfig->debugImageLoading));
    Add(new cMenuEditBoolItem(tr("Limit Logo Cache"), &tmpConfig->limitLogoCache));
    if (&tmpConfig->limitLogoCache) {
        Add(new cMenuEditIntItem(cString::sprintf("%s%s", *indent, tr("Maximal number of logos to cache")), &tmpConfig->numLogosMax, 1, 9999));
    }
    Add(new cMenuEditIntItem(tr("Number of  logos to cache at start"), &tmpConfig->numLogosInitial, 0, 9999));

    Add(InfoItem(tr("Cache Sizes"), ""));
    Add(InfoItem(tr("OSD Element Cache"), cString::sprintf("\t%s", (imgCache.GetCacheSize(ctOsdElement)).c_str())));
    Add(InfoItem(tr("Logo cache"), cString::sprintf("\t%s", (imgCache.GetCacheSize(ctLogo)).c_str())));
    Add(InfoItem(tr("EPG Grid Cache"), cString::sprintf("\t%s", (imgCache.GetCacheSize(ctGrid)).c_str())));
    Add(InfoItem(tr("Channel Groups Cache"), cString::sprintf("\t%s", (imgCache.GetCacheSize(ctChannelGroup)).c_str())));
    Add(InfoItem(tr("Recording Menus Icon Cache"), cString::sprintf("\t%s", (imgCache.GetCacheSize(ctIcon)).c_str())));

    SetCurrent(Get(currentItem));
    Display();
}
