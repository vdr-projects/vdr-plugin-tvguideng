#ifndef __TVGUIDE_OSDMANAGER_H
#define __TVGUIDE_OSDMANAGER_H

#include <vdr/osd.h>

class cOsdManager {
private:
    cOsd *osd;
    cMutex mutex;
public:
    cOsdManager(void);
    void Lock(void);
    void Unlock(void);
    bool CreateOsd(void);
    void DeleteOsd(void);
    void SetBackground(void);
    cPixmap *CreatePixmap(int Layer, const cRect &ViewPort, const cRect &DrawPort = cRect::Null);
    void DestroyPixmap(cPixmap *pixmap);
    void Flush(void);
    int Width(void) { return osd->Width(); };
    int Height(void) { return osd->Height(); };
    int Top(void) { return osd->Top(); };
    int Left(void) { return osd->Left(); };
};

#endif //__TVGUIDE_OSDMANAGER_H
