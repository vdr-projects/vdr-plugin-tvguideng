#include "services/remotetimers.h"
#include "tools.h"
#include "recmenuview.h"
#include "recmenus.h"

// --- cRecMenuMain  ---------------------------------------------------------
cRecMenuMain::cRecMenuMain(bool epgSearchAvailable, bool timerActive, bool switchTimerActive) {
    eRecMenuState action = rmsInstantRecord;
    if (!timerActive) {
        if (config.instRecFolderMode == eFolderSelect)
            action = rmsInstantRecordFolder;
        AddMenuItem(new cRecMenuItemButton(tr("Instant Record"), action, true, false, false, true));
    } else {
        AddMenuItem(new cRecMenuItemButton(tr("Timer On/Off"), rmsOnOffTimer, true, false, false, true));
        AddMenuItem(new cRecMenuItemButton(tr("Edit Timer"), rmsEditTimer, false, false, false, true));
        AddMenuItem(new cRecMenuItemButton(tr("Delete Timer"), rmsDeleteTimer, false, false, false, true));
    }

    AddMenuItem(new cRecMenuItemButton(tr("Timer Timeline"), rmsTimeline, false, false, false, true));

    if (epgSearchAvailable) {
        AddMenuItem(new cRecMenuItemButton(tr("Create Search Timer"), rmsSearchTimer, false, false, false, true));
        AddMenuItem(new cRecMenuItemButton(tr("Search Timers"), rmsSearchTimers, false, false, false, true));
    }

    if (config.instRecFolderMode == eFolderSelect)
        action = rmsSeriesTimerFolder;
    else
        action = rmsSeriesTimer;
    AddMenuItem(new cRecMenuItemButton(tr("Create Series Timer"), action, false, false, false, true));

    if (epgSearchAvailable) {
        if (!switchTimerActive) {
            AddMenuItem(new cRecMenuItemButton(tr("Create Switch Timer"), rmsSwitchTimer, false, false, false, true));
        } else {
            AddMenuItem(new cRecMenuItemButton(tr("Delete Switch Timer"), rmsSwitchTimerDelete, false, false, false, true));
        }
        AddMenuItem(new cRecMenuItemButton(tr("Search"), rmsSearch, false, false, false, true));
    }

    if (epgSearchAvailable) {
        AddMenuItem(new cRecMenuItemButton(tr("Check for Timer Conflicts"), rmsTimerConflicts, false, false, false, true));
    }

    AddMenuItem(new cRecMenuItemButton(tr("Search in Recordings"), rmsRecordingSearch, false, false, false, true));

    int menuWidth = CalculateOptimalWidth() + 4 * border;
    SetWidthPixel(menuWidth);
            
    CalculateHeight();
    CreatePixmap();
    Arrange();
};

/******************************************************************************************
*   Instant Timer Menus
******************************************************************************************/

// --- cRecMenuAskFolder  ---------------------------------------------------------
cRecMenuAskFolder::cRecMenuAskFolder(const cEvent *event, eRecMenuState nextAction) { // OK
    SetWidthPercent(80);

    NextAction = nextAction;

    const cString line1 = tr("Set Folder for");
    const cString line2 = (event && event->Title()) ? cString::sprintf("\"%s\"", event->Title()) : "";

    AddHeader(new cRecMenuItemInfo(*line1, 2, *line2, "", "", width - 2 * border, true));
    AddFooter(new cRecMenuItemButton(tr("root video folder"), nextAction, true, false, true));

    AddMenuItemInitial(new cRecMenuItemButton(tr("root video folder"), nextAction, true, false, true));
 
    ReadRecordingDirectories(&folders, NULL, "");
    int numFolders = folders.size();
    for (int i = 0; i < numFolders; i++) {
        if (!AddMenuItemInitial(new cRecMenuItemButton(folders[i].c_str(), nextAction, false, false, true)))
            break;
    }

    CalculateHeight();
    CreatePixmap();
    Arrange();
}

cRecMenuItem *cRecMenuAskFolder::GetMenuItem(int number) {
    if (number == 0) {
        cRecMenuItem *result = new cRecMenuItemButton(tr("root video folder"), rmsInstantRecord, false, false, true);
        return result;
    } else if ((number > 0) && (number < (int)folders.size() + 1)) {
        cRecMenuItem *result = new cRecMenuItemButton(folders[number-1].c_str(), rmsInstantRecord, false, false, true);
        return result;
    }
    return NULL;
}

int cRecMenuAskFolder::GetTotalNumMenuItems(void) { 
    return folders.size() + 1;
}

std::string cRecMenuAskFolder::GetFolder(void) {
    std::string folder = "";
    int folderActive = GetNumActive();
    if (folderActive > 0 && folderActive < (int)folders.size() + 1)
        folder = folders[folderActive - 1];
    return folder;
}

// --- cRecMenuConfirmTimer  ---------------------------------------------------------
cRecMenuConfirmTimer::cRecMenuConfirmTimer(const cEvent *event, bool timerChanged) { // OK
    SetWidthPercent(50);

    bool eventHasTimer = false;
    if (config.useRemoteTimers && pRemoteTimers) {
        RemoteTimers_GetMatch_v1_0 rtMatch;
        rtMatch.event = event;
        pRemoteTimers->Service("RemoteTimers::GetMatch-v1.0", &rtMatch);
        if (rtMatch.timerMatch == tmFull) {
            eventHasTimer = true;
        }
    } else {
        eventHasTimer = event->HasTimer();
    }

    const cChannels *channels = NULL;
#if VDRVERSNUM >= 20301
    {
    LOCK_CHANNELS_READ;
    channels = Channels;
    }
#else
    channels = &Channels;
#endif
    const cString channelName = channels->GetByChannelID(event->ChannelID())->Name();
    const cString line1 = (eventHasTimer) ? (timerChanged) ? tr("Timer changed")
                                                           : tr("Timer created")
                                          : tr("Timer NOT created");
    const cString line3 = cString::sprintf("%s %s - %s", *event->GetDateString(), *event->GetTimeString(), *event->GetEndTimeString());
    const cString line4 = (event && event->Title()) ? cString::sprintf("\"%s\"", event->Title()) : "";

    AddHeader(new cRecMenuItemInfo(*line1, 4, *channelName, *line3, *line4, width - 2 * border));
    AddFooter(new cRecMenuItemButton(tr("OK"), rmsClose, true, true));

    CalculateHeight();
    CreatePixmap();
    Arrange();
}

// --- cRecMenuConfirmDeleteTimer  ---------------------------------------------------------
cRecMenuConfirmDeleteTimer::cRecMenuConfirmDeleteTimer(const cEvent *event) { // OK
    SetWidthPercent(50);

    const cChannels *channels = NULL;
#if VDRVERSNUM >= 20301
    {
    LOCK_CHANNELS_READ;
    channels = Channels;
    }
#else
    channels = &Channels;
#endif
    const cString channelName = channels->GetByChannelID(event->ChannelID())->Name();
    const cString line1 = tr("Timer deleted");
    const cString line3 = cString::sprintf("%s %s - %s", *event->GetDateString(), *event->GetTimeString(), *event->GetEndTimeString());
    const cString line4 = (event && event->Title()) ? cString::sprintf("\"%s\"", event->Title()) : "";

    AddHeader(new cRecMenuItemInfo(*line1, 4, *channelName, *line3, *line4, width - 2 * border));
    AddFooter(new cRecMenuItemButton(tr("OK"), rmsClose, true, true));

    CalculateHeight();
    CreatePixmap();
    Arrange();
}

// --- cRecMenuAskDeleteTimer ---------------------------------------------------------
cRecMenuAskDeleteTimer::cRecMenuAskDeleteTimer(const cEvent *event) { // OK
    SetWidthPercent(50);

    const cChannels *channels = NULL;
#if VDRVERSNUM >= 20301
    {
    LOCK_CHANNELS_READ;
    channels = Channels;
    }
#else
    channels = &Channels;
#endif
    const cString channelName = channels->GetByChannelID(event->ChannelID())->Name();
    const cString line1 = tr("Timer");
    const cString line3 = (event && event->Title()) ? cString::sprintf("\"%s\"", event->Title()) : "";
    const cString line4 = tr("still recording - really delete?");

    AddHeader(new cRecMenuItemInfo(*line1, 4, *channelName, *line3, *line4, width - 2 * border));
    AddFooter(new cRecMenuItemButtonYesNo(tr("Yes"), tr("No"), rmsDeleteTimerConfirmation, rmsClose, true));

    CalculateHeight();
    CreatePixmap();
    Arrange();
}

// --- cRecMenuTimerConflicts  ---------------------------------------------------------
cRecMenuTimerConflicts::cRecMenuTimerConflicts(cTVGuideTimerConflicts *conflicts) { // OK
    SetWidthPercent(50);

    int numConflicts = (conflicts) ? conflicts->NumConflicts() : 0;

    const cString line1 = cString::sprintf("%d %s %s",
                                           numConflicts,
                                           (numConflicts == 1) ? tr("Timer Conflict") : tr("Timer Conflicts"),
                                           tr("detected"));
    AddHeader(new cRecMenuItemInfo(*line1, 1, "", "", "", width - 2 * border, true));
    AddFooter(new cRecMenuItemButton(tr("Ignore Conflicts"), rmsClose, false));

    for (int i = 0; i < numConflicts; i++) {
        cTVGuideTimerConflict *conflict = conflicts->GetConflict(i);
        if (!conflict)
            continue;
        const cString dateTime = DayDateTime(conflict->time);
        int numTimers = conflict->timerIDs.size();
        AddMenuItem(new cRecMenuItemButton(cString::sprintf("%s: %s (%d %s)",
                                                            tr("Show conflict"),
                                                            *dateTime,
                                                            numTimers,
                                                            tr("timers involved")),
                                           rmsTimerConflict, (i == 0) ? true : false));
    }

    SetWidthPixel(CalculateOptimalWidth() + 4 * border);

    CalculateHeight();
    CreatePixmap();
    Arrange();
}

int cRecMenuTimerConflicts::GetTimerConflict(void) {
    return GetNumActive();
}

// --- cRecMenuTimerConflict  ---------------------------------------------------------
cRecMenuTimerConflict::cRecMenuTimerConflict(cTVGuideTimerConflict *conflict, eRecMenuState nextAction) { //OK
    SetWidthPercent(90);

    this->conflict = conflict;
    totalNumMenuItems = (int)conflict->timerIDs.size();

    AddHeader(new cRecMenuItemTimerConflictHeader(conflict->timeStart,
                                                  conflict->timeStop,
                                                  conflict->overlapStart,
                                                  conflict->overlapStop));
    AddFooter(new cRecMenuItemButton(tr("Ignore Conflict"), nextAction, false, true));

    int i = 0;
    for(std::vector<int>::iterator it = conflict->timerIDs.begin(); it != conflict->timerIDs.end(); it++) {
#if VDRVERSNUM >= 20301
        LOCK_TIMERS_READ;
        const cTimer *timer = Timers->GetById(*it + 1);
#else
        const cTimer *timer = Timers.Get(*it);
#endif
        if (timer) {
            if (!AddMenuItemInitial(new cRecMenuItemTimer(timer,
                                                          rmsTimerConflictShowInfo,
                                                          rmsDeleteTimerConflictMenu,
                                                          rmsEditTimerConflictMenu,
                                                          rmsSearchRerunsTimerConflictMenu,
                                                          conflict->timeStart,
                                                          conflict->timeStop,
                                                          conflict->overlapStart,
                                                          conflict->overlapStop,
                                                          (!i) ? true : false)))
                break;
            i++;
        }
    }

    CalculateHeight();
    CreatePixmap();
    Arrange();
}

cRecMenuItem *cRecMenuTimerConflict::GetMenuItem(int number) { 
    if ((number >= 0) && (number < totalNumMenuItems)) {
#if VDRVERSNUM >= 20301
        LOCK_TIMERS_READ;
        const cTimer *timer = Timers->Get(conflict->timerIDs[number]);
#else
        const cTimer *timer = Timers.Get(conflict->timerIDs[number]);
#endif
        cRecMenuItem *result = new cRecMenuItemTimer(timer,
                                                     rmsTimerConflictShowInfo,
                                                     rmsDeleteTimerConflictMenu, 
                                                     rmsEditTimerConflictMenu,
                                                     rmsSearchRerunsTimerConflictMenu,
                                                     conflict->timeStart, 
                                                     conflict->timeStop, 
                                                     conflict->overlapStart, 
                                                     conflict->overlapStop, 
                                                     false);
        return result;
    }
    return NULL;
}

int cRecMenuTimerConflict::GetTotalNumMenuItems(void) {
    return totalNumMenuItems; 
}

int cRecMenuTimerConflict::GetTimerConflictIndex(void) {
    return GetNumActive();
}

// --- cRecMenuNoTimerConflict ---------------------------------------------------------
cRecMenuNoTimerConflict::cRecMenuNoTimerConflict(void) { // OK
    SetWidthPercent(50);

    const cString line1 = tr("No Timer Conflicts found");

    AddHeader(new cRecMenuItemInfo(*line1, 1, "", "", "", width - 4 * border));
    AddFooter(new cRecMenuItemButton(tr("Close"), rmsClose, true));

    CalculateHeight();
    CreatePixmap();
    Arrange();
}

// --- cRecMenuRerunResults ---------------------------------------------------------
cRecMenuRerunResults::cRecMenuRerunResults(const cEvent *original, const cEvent **reruns, int numReruns) { // OK
    SetWidthPercent(70);

    this->reruns = reruns;
    this->numReruns = numReruns;

    const cString line1 = cString::sprintf("%d %s:", numReruns, (numReruns > 1) ? tr("reruns for") : tr("rerun for"));
    const cString line2 = cString::sprintf("\"%s\"", original->Title());
    const cString line3 = tr("found");

    AddHeader(new cRecMenuItemInfo(*line1, 3, *line2, *line3, "", width - 2 * border, true));
    AddFooter(new cRecMenuItemButton(tr("Ignore reruns"), rmsTimerConflictIgnoreReruns, false));

    if (reruns && (numReruns > 0)) {
        for (int i = 0; i < numReruns; i++) {
            if (!AddMenuItemInitial(new cRecMenuItemEvent(reruns[i], rmsSearchShowInfo, rmsTimerConflictRecordRerun, (i == 0) ? true : false)))
                break;
        }
    }

    CalculateHeight();
    CreatePixmap();
    Arrange();
}

cRecMenuItem *cRecMenuRerunResults::GetMenuItem(int number) {
   if ((number >= 0) && (number < numReruns)) {
        cRecMenuItem *result = new cRecMenuItemEvent(reruns[number], rmsSearchShowInfo, rmsTimerConflictRecordRerun, false);
        return result;
    }
    return NULL;
}

int cRecMenuRerunResults::GetTotalNumMenuItems(void) {
    return numReruns;
}

const cEvent *cRecMenuRerunResults::GetRerunEvent(void) {
    if (cRecMenuItemEvent *activeItem = dynamic_cast<cRecMenuItemEvent*>(GetActiveMenuItem()))
        return activeItem->GetEvent();
    return NULL;
}

// --- cRecMenuNoRerunsFound  ---------------------------------------------------------
cRecMenuNoRerunsFound::cRecMenuNoRerunsFound(cString searchString) { // OK
    SetWidthPercent(50);

    const cString line1 = tr("No reruns found for Event");
    const cString eventQuoted = cString::sprintf("\"%s\"", *searchString);

    AddHeader(new cRecMenuItemInfo(*line1, 2, *eventQuoted, "", "", width - 2 * border));
    AddFooter(new cRecMenuItemButton(tr("OK"), rmsTimerConflictIgnoreReruns, true, true));

    CalculateHeight();
    CreatePixmap();
    Arrange();
}

// --- cRecMenuConfirmRerunUsed  ---------------------------------------------------------
cRecMenuConfirmRerunUsed::cRecMenuConfirmRerunUsed(const cEvent *original, const cEvent *replace) { // OK
    SetWidthPercent(70);

    const cChannels *channels = NULL;
#if VDRVERSNUM >= 20301
    {
    LOCK_CHANNELS_READ;
    channels = Channels;
    }
#else
    channels = &Channels;
#endif
    const cString channelOrig = channels->GetByChannelID(original->ChannelID())->Name();
    const cString channelReplace = channels->GetByChannelID(replace->ChannelID())->Name();

    const cString line1 = tr("Timer for");
    const cString line2 = *cString::sprintf("\"%s\", %s %s, %s", original->Title(), *original->GetDateString(), *original->GetTimeString(), *channelOrig);
    const cString line3 = tr("replaced by rerun");
    const cString line4 = *cString::sprintf("\"%s\", %s %s, %s", replace->Title(), *replace->GetDateString(), *replace->GetTimeString(), *channelReplace);

    AddHeader(new cRecMenuItemInfo(*line1, 4, *line2, *line3, *line4, width - 2 * border));
    AddFooter(new cRecMenuItemButton(tr("OK"), rmsTimerConflicts, true, true));

    CalculateHeight();
    CreatePixmap();
    Arrange();
}

// --- cRecMenuEditTimer  ---------------------------------------------------------
cRecMenuEditTimer::cRecMenuEditTimer(const cTimer *timer, eRecMenuState nextState) { // OK
    if (!timer)
        return;

    SetWidthPercent(70);
    originalTimer = timer;
    timerActive = false;

    const cString line1 = tr("Edit Timer");
    const cString line2 = (timer->Event() && timer->Event()->Title()) ? timer->Event()->Title() : "";
    const cString line3 = (timer->Channel() && timer->Channel()->Name()) ? timer->Channel()->Name() : "";

    AddHeader(new cRecMenuItemInfo(*line1, 3, *line2, *line3, "", width - 2 * border, true));
    AddFooter(new cRecMenuItemButtonYesNo(tr("Save"), tr("Cancel"), nextState, (nextState == rmsTimelineTimerSave) ? rmsTimeline : rmsClose, false));

    if (config.useRemoteTimers && pRemoteTimers) {
        RemoteTimers_GetMatch_v1_0 rtMatch;
        rtMatch.event = timer->Event();
        pRemoteTimers->Service("RemoteTimers::GetMatch-v1.0", &rtMatch);
        if (rtMatch.timer) {
            if (rtMatch.timerMatch == tmFull)
                timerActive = true;
        }
    } else
        timerActive = timer->HasFlags(tfActive);

    day = timer->Day();
    start = timer->Start();
    stop = timer->Stop();
    prio = timer->Priority();
    lifetime = timer->Lifetime();
    strncpy(folder, GetDirectoryFromTimer(timer->File()).c_str(), TEXTINPUTLENGTH);

    AddMenuItem(new cRecMenuItemBool(tr("Timer Active"), true, &timerActive));
    AddMenuItem(new cRecMenuItemInt(tr("Priority"), 0, MAXPRIORITY, false, &prio));
    AddMenuItem(new cRecMenuItemInt(tr("Lifetime"), 0, MAXLIFETIME, false, &lifetime));
    AddMenuItem(new cRecMenuItemDay(tr("Day"), false, &day));
    AddMenuItem(new cRecMenuItemTime(tr("Timer start time"), false, &start));
    AddMenuItem(new cRecMenuItemTime(tr("Timer stop time"), false, &stop));
    AddMenuItem(new cRecMenuItemInfo(tr("Timer File"), 2, timer->File() ? timer->File() : "",  "", "", width - 2 * border, false));
    AddMenuItem(new cRecMenuItemSelectDirectory(tr("New Folder"), std::string(folder), false, folder));

    if (nextState == rmsTimelineTimerSave)
        AddMenuItem(new cRecMenuItemButton(tr("Delete Timer"), rmsTimelineTimerDelete, false));

    CalculateHeight();
    CreatePixmap();
    Arrange();
}

const cTimer *cRecMenuEditTimer::GetOriginalTimer(void) {
    return originalTimer;
}

cTimer *cRecMenuEditTimer::GetTimer(void) {
    cTimer *t = (cTimer *)originalTimer;
    if (timerActive)
        t->SetFlags(tfActive);
    else 
        t->ClrFlags(tfActive);
    t->SetDay(day);
    t->SetStart(start);
    t->SetStop(stop);
    t->SetPriority(prio);
    t->SetLifetime(lifetime);
    std::string newFolder(folder);
    std::string newFile = originalTimer->File();
    size_t found = newFile.find_last_of('~');
    if (found != std::string::npos) {
        std::string fileName = newFile.substr(found + 1);
        if (newFolder.size() > 0)
            newFile = *cString::sprintf("%s~%s", newFolder.c_str(), fileName.c_str());
        else
            newFile = fileName;
    } else {
        if (newFolder.size() > 0)
            newFile = *cString::sprintf("%s~%s", newFolder.c_str(), newFile.c_str());
    }
    std::replace(newFile.begin(), newFile.end(), '/', '~');
    t->SetFile(newFile.c_str());
    return t;
}

/******************************************************************************************
*   Series Timer Menus
******************************************************************************************/

// --- cRecMenuSeriesTimer ---------------------------------------------------------
cRecMenuSeriesTimer::cRecMenuSeriesTimer(const cChannel *initialChannel, const cEvent *event, std::string folder) { // OK
    if (!initialChannel)
        return;

    SetWidthPercent(70);

    timerActive = true;
    channel = initialChannel->Number();
    dayOfWeek = 127; 
    priority =  MAXPRIORITY;
    lifetime = MAXLIFETIME;
    CalculateTimes(event);
    this->folder = folder;

    const cString line1 = tr("Create Series Timer based on:");
    const cString line2 = cString::sprintf("\"%s\"", event->Title());

    AddHeader(new cRecMenuItemInfo(*line1, 2, *line2, "", "", width - 2 * border, true));
    AddFooter(new cRecMenuItemButtonYesNo(tr("Create Timer"), tr("Cancel"), rmsSeriesTimerCreate, rmsClose, true));

    AddMenuItem(new cRecMenuItemBool(tr("Timer Active"), false, &timerActive));
    AddMenuItem(new cRecMenuItemChannelChooser(tr("Channel"), false, &channel));
    AddMenuItem(new cRecMenuItemTime(tr("Series Timer start time"), false, &start));
    AddMenuItem(new cRecMenuItemTime(tr("Series Timer stop time"), false, &stop));
    AddMenuItem(new cRecMenuItemDayChooser(tr("Days to record"), false, &dayOfWeek));
    AddMenuItem(new cRecMenuItemDay(tr("Day to start"), false, &tstart));
    AddMenuItem(new cRecMenuItemInt(tr("Priority"), 0, MAXPRIORITY, false, &priority));
    AddMenuItem(new cRecMenuItemInt(tr("Lifetime"), 0, MAXLIFETIME, false, &lifetime));

    CalculateHeight();
    CreatePixmap();
    Arrange();
}

cTimer *cRecMenuSeriesTimer::GetTimer(void) {
#if VDRVERSNUM >= 20301
    LOCK_CHANNELS_READ;
    const cChannel *chan = Channels->GetByNumber(channel);
#else
    cChannel *chan = Channels.GetByNumber(channel);
#endif
    cTimer *seriesTimer = new cTimer(NULL, NULL, chan);
    cString fileName = "TITLE EPISODE";
    if (folder.size() > 0) {
        std::replace(folder.begin(), folder.end(), '/', '~');
        fileName = cString::sprintf("%s~%s", folder.c_str(), *fileName);
    }
    seriesTimer->SetDay(tstart);
    seriesTimer->SetStart(start);
    seriesTimer->SetStop(stop);
    seriesTimer->SetPriority(priority);
    seriesTimer->SetLifetime(lifetime);
    seriesTimer->SetWeekDays(dayOfWeek);
    seriesTimer->SetFile(*fileName);
    if (timerActive)
        seriesTimer->SetFlags(tfActive);
    else 
        seriesTimer->SetFlags(tfNone);
    return seriesTimer;
}

void cRecMenuSeriesTimer::CalculateTimes(const cEvent *event) {
    tstart = event->StartTime();
    tstart -= Setup.MarginStart * 60;
    time_t tstop = tstart + event->Duration();
    tstop  += Setup.MarginStop * 60;
    
    struct tm tm_r;
    struct tm *time = localtime_r(&tstart, &tm_r);
    start = time->tm_hour * 100 + time->tm_min;
    time = localtime_r(&tstop, &tm_r);
    stop = time->tm_hour * 100 + time->tm_min;
    if (stop >= 2400)
        stop -= 2400;
}

// --- cRecMenuConfirmSeriesTimer  ---------------------------------------------------------
cRecMenuConfirmSeriesTimer::cRecMenuConfirmSeriesTimer(cTimer *seriesTimer) { // OK
    SetWidthPercent(50);

    int i = 1;
    cString line2 = "";
    const cString line1 = tr("Series Timer created");
    if (seriesTimer) {
        const cString days = cTimer::PrintDay(seriesTimer->Day(), seriesTimer->WeekDays(), true);
        line2 = cString::sprintf("%s, %s: %s, %s: %s", *days, tr("Start"), *TimeString(seriesTimer->StartTime()), tr("Stop"), *TimeString(seriesTimer->StopTime()));
        i = 2;
    }

    AddHeader(new cRecMenuItemInfo(*line1, i, *line2, "", "", width - 2 * border, true));
    AddFooter(new cRecMenuItemButton(tr("OK"), rmsClose, true, true));

    CalculateHeight();
    CreatePixmap();
    Arrange();
}

/******************************************************************************************
*   SearchTimer Menus
******************************************************************************************/

// --- cRecMenuSearchTimer  ---------------------------------------------------------
cRecMenuSearchTimer::cRecMenuSearchTimer(const cEvent *event) { // OK
    SetWidthPercent(70);

    const cString line1 = tr("Configure Search Timer based on:");
    const cString line2 = cString::sprintf("\"%s\"", event->Title());

    AddHeader(new cRecMenuItemInfo(*line1, 2, *line2, "", "", width - 2 * border, true));
    AddFooter(new cRecMenuItemButtonYesNo(tr("Continue"), tr("Cancel"), rmsSearchTimerOptions, rmsClose, true));

    strncpy(searchString, event->Title(), TEXTINPUTLENGTH);
    AddMenuItemInitial(new cRecMenuItemText(tr("Search Expression:"), TEXTINPUTLENGTH, false, searchString));

    CalculateHeight();
    CreatePixmap();
    Arrange();
}

// --- cRecMenuSearchTimerTemplates  ---------------------------------------------------------
cRecMenuSearchTimerTemplates::cRecMenuSearchTimerTemplates(cTVGuideSearchTimer searchTimer, std::vector<TVGuideEPGSearchTemplate> templates) { // OK
    SetWidthPercent(70);

    this->searchTimer = searchTimer;
    this->templates = templates;

    const cString line1 = tr("Configure Search Timer for Search String:");
    const cString line2 = cString::sprintf("\"%s\"", searchTimer.GetSearchString().c_str());

    AddHeader(new cRecMenuItemInfo(*line1, 2, *line2, "", "", width - 2 * border, true));
    AddFooter(new cRecMenuItemButton(tr("Manually configure Options"), rmsSearchTimerEdit, true));

    numTemplates = templates.size();
    for (int i = 0; i < numTemplates; i++) {
        const cString buttonText = cString::sprintf("%s \"%s\"", tr("Use Template"), templates[i].name.c_str());
        if (!AddMenuItemInitial(new cRecMenuItemButton(*buttonText, rmsSearchTimerCreateWithTemplate, false)))
            break;
    }

    CalculateHeight();
    CreatePixmap();
    Arrange();
}

cRecMenuItem *cRecMenuSearchTimerTemplates::GetMenuItem(int number) {
    if (number == 0) {
        return new cRecMenuItemButton(tr("Manually configure Options"), rmsSearchTimerEdit, false);
    } else if ((number > 0) && (number < numTemplates + 1)) {
        cString buttonText = cString::sprintf("%s \"%s\"", tr("Use Template"), templates[number-1].name.c_str());
        cRecMenuItem *templ = new cRecMenuItemButton(*buttonText, rmsSearchTimerCreateWithTemplate, false);
        return templ;
    }
    return NULL;
}

int cRecMenuSearchTimerTemplates::GetTotalNumMenuItems(void) { 
    return numTemplates + 1;
}

TVGuideEPGSearchTemplate cRecMenuSearchTimerTemplates::GetTemplate(void) {
    TVGuideEPGSearchTemplate templ;
    int tmplActive = GetNumActive() - 1;
    if (tmplActive >= 0 && tmplActive < (int)templates.size())
        templ = templates[tmplActive];
    return templ;
}

// --- cRecMenuSearchTimers  ---------------------------------------------------------
cRecMenuSearchTimers::cRecMenuSearchTimers(std::vector<cTVGuideSearchTimer> searchTimers) { // OK
    SetWidthPercent(70);

    this->searchTimers = searchTimers;
    numSearchTimers = searchTimers.size();

    const cString line1 = (numSearchTimers > 0) ? tr("EPGSearch Search Timers") : tr("No Search Timers Configured");

    AddHeader(new cRecMenuItemInfo(*line1, 1, "", "", "", width - 2 * border, true));
    AddFooter(new cRecMenuItemButton(tr("Close"), rmsClose, (!numSearchTimers) ? true : false));

    if (numSearchTimers > 0) {
        for (int i = 0; i < numSearchTimers; i++) {
            if (!AddMenuItemInitial(new cRecMenuItemSearchTimer(searchTimers[i], rmsSearchTimerTest, rmsSearchTimerEdit, rmsSearchTimerDeleteConfirm, (i == 0) ? true : false)))
                break;
        }
    }

    CalculateHeight();
    CreatePixmap();
    Arrange();
}

cTVGuideSearchTimer cRecMenuSearchTimers::GetSearchTimer(void) {
    cRecMenuItemSearchTimer *activeItem = dynamic_cast<cRecMenuItemSearchTimer*>(GetActiveMenuItem());
    return activeItem->GetTimer();
}

cRecMenuItem *cRecMenuSearchTimers::GetMenuItem(int number) {
    if (number < 0 || number >= numSearchTimers)
        return NULL;
    return new cRecMenuItemSearchTimer(searchTimers[number], rmsSearchTimerTest, rmsSearchTimerEdit, rmsSearchTimerDeleteConfirm, false);
}

int cRecMenuSearchTimers::GetTotalNumMenuItems(void) {
    return numSearchTimers;
}

// --- cRecMenuSearchTimerEdit  ---------------------------------------------------------
cRecMenuSearchTimerEdit::cRecMenuSearchTimerEdit(cTVGuideSearchTimer searchTimer, std::vector<std::string> channelGroups) { // OK
    SetWidthPercent(70);

    init = true;
    deleteMenuItems = false;
    this->sT = searchTimer;
    this->channelGroups = channelGroups;
    strncpy(searchString, sT.searchString.c_str(), TEXTINPUTLENGTH);
    channelgroupIndex = -1;
    std::string dir = sT.directory;
    strncpy(directory, dir.c_str(), TEXTINPUTLENGTH);
    dayOfWeek = DayOfWeek(sT.dayOfWeek);
    indent = "   ";

    if (sT.useChannel == 1) {
        startChannel = (sT.channelMin) ? sT.channelMin->Number() : 0;
        stopChannel = (sT.channelMax) ? sT.channelMax->Number() : 0;
        if (startChannel == 0) startChannel = 1;
        if (stopChannel == 0) stopChannel = 1;
    }

    sT.GetSearchModes(&searchModes);
    sT.GetSubTitleModes(&subTitleModes);
    sT.GetUseChannelModes(&useChannelModes);
    sT.GetCompareDateModes(&compareDateModes);
    sT.GetSearchTimerModes(&searchTimerModes);
    sT.GetDelModes(&delModes);
    channelgroupIndex = SplitChannelGroups(&channelGroups, &channelgroups);

    const cString line1 = (sT.GetID() > -1) ? tr("Configure Search Timer Options") : tr("Create Search Timer");

    AddHeader(new cRecMenuItemInfo(*line1, 1, "", "", "", width - 2 * border, true));
    AddFooter(new cRecMenuItemButtonYesNo(tr("Save Search Timer"), tr("Cancel"), rmsSearchTimerSave, rmsSearchTimers, false));

    CreateMenuItems();
}

cRecMenuSearchTimerEdit::~cRecMenuSearchTimerEdit(void) {
    for (std::vector<cRecMenuItem*>::iterator it = mainMenuItems.begin(); it != mainMenuItems.end(); it++) {
        delete *it;
    }
    mainMenuItems.clear();
}

int cRecMenuSearchTimerEdit::DayOfWeek(int dayofWeek) {
    int vdrDayOfWeek = 0;
    if (dayofWeek > 0) {
        vdrDayOfWeek = pow(2, (dayofWeek + 6) % 7);
    } else if (dayofWeek < 0) {
        int absDayOfWeek = abs(dayofWeek);
        for (int i = 0; i < 7; i++) {
            if (absDayOfWeek & (1 << i)) {
                vdrDayOfWeek += pow(2, (i + 6) % 7);
            }
        }
    }
    return vdrDayOfWeek;
}

int cRecMenuSearchTimerEdit::SetDayOfWeek(int VDRDayOfWeek) {
    int epgSearchDayOfWeek = 0;
    for (int i = 0; i < 7; i++) {
        if (VDRDayOfWeek & (1 << i)) {
            epgSearchDayOfWeek += pow(2, (i + 1) % 7);
        }
    }
    return epgSearchDayOfWeek * (-1);
}

int cRecMenuSearchTimerEdit::SplitChannelGroups(std::vector<std::string> *channelGroups, std::vector<std::string> *channelgroups) {
    int i = 0;
    int j = 0;
    for (std::vector<std::string>::iterator it = channelGroups->begin(); it != channelGroups->end(); it++) {
        std::string a = *it;
        splitstring s(a.c_str());
        std::vector<std::string> value = s.split('|', 0);
        std::vector<std::string>::iterator ito = value.begin();
        channelgroups->push_back(*ito);
        std::string b = *ito;
        if (b.compare(sT.channelGroup) == 0)
            j = i;
        i++;
    }
    return j;
}

void cRecMenuSearchTimerEdit::CreateMenuItems(void) {

    int activeMenuItem = 0;

    if (mainMenuItems.size() > 0) {
        for (long unsigned int index = 0; index < mainMenuItems.size(); index++) {
            if (mainMenuItems[index]->IsActive()) {
                activeMenuItem = index;
                break;
            }
        }
        mainMenuItems.clear();
    }

    mainMenuItems.push_back(new cRecMenuItemText(tr("Search term"), TEXTINPUTLENGTH, init, searchString));
    mainMenuItems.push_back(new cRecMenuItemBool(tr("Active"), false, &sT.useAsSearchTimer, rmsSearchTimerSave, true));
    mainMenuItems.push_back(new cRecMenuItemSelect(tr("Search mode"), searchModes, false, &sT.mode, rmsSearchTimerSave, true));
    if (sT.mode == 5) {
        mainMenuItems.push_back(new cRecMenuItemInt(cString::sprintf("%s%s", *indent, tr("Tolerance")), 1, 9, false, &sT.fuzzyTolerance, rmsSearchTimerSave));
    }
    mainMenuItems.push_back(new cRecMenuItemBool(tr("Match case"), false, &sT.useCase, rmsSearchTimerSave));
    mainMenuItems.push_back(new cRecMenuItemBool(tr("Use title"), false, &sT.useTitle, rmsSearchTimerSave));
    mainMenuItems.push_back(new cRecMenuItemBool(tr("Use subtitle"), false, &sT.useSubtitle, rmsSearchTimerSave));
    mainMenuItems.push_back(new cRecMenuItemBool(tr("Use description"), false, &sT.useDescription, rmsSearchTimerSave));
//    mainMenuItems.push_back(new cRecMenuItemBool(tr("Use content descriptor"), false, &sT.useContentDescriptors, rmsSearchTimerSave));
    mainMenuItems.push_back(new cRecMenuItemSelect(tr("Use channel"), useChannelModes, false, &sT.useChannel, rmsSearchTimerSave, true));
    if (sT.useChannel == 1) {
        mainMenuItems.push_back(new cRecMenuItemChannelChooser(cString::sprintf("%s%s", *indent, tr("from channel")), false, &startChannel, rmsSearchTimerSave));
        mainMenuItems.push_back(new cRecMenuItemChannelChooser(cString::sprintf("%s%s", *indent, tr("to channel")), false, &stopChannel, rmsSearchTimerSave));
    }
    else if ((sT.useChannel == 2) && (channelgroups.size() > 0)) {
        mainMenuItems.push_back(new cRecMenuItemSelect(cString::sprintf("%s%s", *indent, tr("Channel group")), channelgroups, false, &channelgroupIndex, rmsSearchTimerSave, false));
    }
    mainMenuItems.push_back(new cRecMenuItemBool(tr("Use time"), false, &sT.useTime, rmsSearchTimerSave, true));
    if (sT.useTime) {
        mainMenuItems.push_back(new cRecMenuItemTime(cString::sprintf("%s%s", *indent, tr("Start after")), false, &sT.startTime, rmsSearchTimerSave));
        mainMenuItems.push_back(new cRecMenuItemTime(cString::sprintf("%s%s", *indent, tr("Start before")), false, &sT.stopTime, rmsSearchTimerSave));
    }
    mainMenuItems.push_back(new cRecMenuItemBool(tr("Use duration"), false, &sT.useDuration, rmsSearchTimerSave, true));
    if (sT.useDuration) {
        mainMenuItems.push_back(new cRecMenuItemTime(cString::sprintf("%s%s", *indent, tr("Min. duration")), false, &sT.minDuration, rmsSearchTimerSave));
        mainMenuItems.push_back(new cRecMenuItemTime(cString::sprintf("%s%s", *indent, tr("Max. duration")), false, &sT.maxDuration, rmsSearchTimerSave));
    }
    mainMenuItems.push_back(new cRecMenuItemBool(tr("Use day of week"), false, &sT.useDayOfWeek, rmsSearchTimerSave, true));
    if (sT.useDayOfWeek)
        mainMenuItems.push_back(new cRecMenuItemDayChooser(cString::sprintf("%s%s", *indent, tr("Day of week")), false, &dayOfWeek));
    mainMenuItems.push_back(new cRecMenuItemBool(tr("Use in Favorites"), false, &sT.useInFavorites, rmsSearchTimerSave));
    mainMenuItems.push_back(new cRecMenuItemBool(tr("Use as search timer"), false, &sT.useAsSearchTimer, rmsSearchTimerSave, true));
    if (sT.useAsSearchTimer) {
        mainMenuItems.push_back(new cRecMenuItemSelect(cString::sprintf("%s%s", *indent, tr("Action")), searchTimerModes, false, &sT.action, rmsSearchTimerSave, true));
        if (sT.action == searchTimerActionSwitchOnly) {
            mainMenuItems.push_back(new cRecMenuItemInt(cString::sprintf("%s%s%s", *indent, *indent, tr("Switch ... minutes before start")), 0, 99, false, &sT.switchMinsBefore, rmsSearchTimerSave));
            mainMenuItems.push_back(new cRecMenuItemBool(cString::sprintf("%s%s%s", *indent, *indent, tr("Unmute sound")), false, &sT.unmuteSoundOnSwitch, rmsSearchTimerSave));
        }
        if (sT.action ==  searchTimerActionAnnounceAndSwitch) {
            mainMenuItems.push_back(new cRecMenuItemInt(cString::sprintf("%s%s%s", *indent, *indent, tr("Ask ... minutes before start")), 0, 99, false, &sT.switchMinsBefore, rmsSearchTimerSave));
            mainMenuItems.push_back(new cRecMenuItemBool(cString::sprintf("%s%s%s", *indent, *indent, tr("Unmute sound")), false, &sT.unmuteSoundOnSwitch, rmsSearchTimerSave));
        }
        if ((sT.action == searchTimerActionRecord) || (sT.action == searchTimerActionInactiveRecord)) {
            mainMenuItems.push_back(new cRecMenuItemBool(cString::sprintf("%s%s%s", *indent, *indent, tr("Series Recording")), false, &sT.useEpisode, rmsSearchTimerSave));
            mainMenuItems.push_back(new cRecMenuItemSelectDirectory(cString::sprintf("%s%s%s", *indent, *indent, tr("Directory")), std::string(directory), false, directory, rmsSearchTimerSave, true));
            mainMenuItems.push_back(new cRecMenuItemInt(cString::sprintf("%s%s%s", *indent, *indent, tr("Delete recordings after ... days")), 0, 999, false, &sT.delAfterDays, rmsSearchTimerSave));
            if (sT.delAfterDays > 0) {
                mainMenuItems.push_back(new cRecMenuItemInt(cString::sprintf("%s%s%s%s", *indent, *indent, *indent, tr("Keep ... recordings")), 0, 999, false, &sT.recordingsKeep, rmsSearchTimerSave));
            }
            mainMenuItems.push_back(new cRecMenuItemInt(cString::sprintf("%s%s%s", *indent, *indent, tr("Pause when ... recordings exist")), 0, 999, false, &sT.pauseOnNrRecordings, rmsSearchTimerSave));
            mainMenuItems.push_back(new cRecMenuItemBool(cString::sprintf("%s%s%s", *indent, *indent, tr("Avoid Repeats")), false, &sT.avoidRepeats, rmsSearchTimerSave, true));
            if (sT.avoidRepeats) {
                mainMenuItems.push_back(new cRecMenuItemInt(cString::sprintf("%s%s%s%s", *indent, *indent, *indent, tr("Allowed repeats")), 0, 99, false, &sT.allowedRepeats, rmsSearchTimerSave));
                if (sT.allowedRepeats > 0) {
                    mainMenuItems.push_back(new cRecMenuItemInt(cString::sprintf("%s%s%s%s%s", *indent, *indent, *indent, *indent, tr("Only repeats within ... days")), 0, 999, false, &sT.repeatsWithinDays, rmsSearchTimerSave));
                }
                mainMenuItems.push_back(new cRecMenuItemBool(cString::sprintf("%s%s%s%s", *indent, *indent, *indent, tr("Compare Title")), false, &sT.compareTitle, rmsSearchTimerSave));
                mainMenuItems.push_back(new cRecMenuItemSelect(cString::sprintf("%s%s%s%s", *indent, *indent, *indent, tr("Compare Subtitle")), subTitleModes, false, &sT.compareSubtitle, rmsSearchTimerSave));
                mainMenuItems.push_back(new cRecMenuItemBool(cString::sprintf("%s%s%s%s", *indent, *indent, *indent, tr("Compare Description")), false, &sT.compareSummary, rmsSearchTimerSave, true));
                if (sT.compareSummary) {
                    mainMenuItems.push_back(new cRecMenuItemInt(cString::sprintf("%s%s%s%s%s", *indent, *indent, *indent, *indent, tr("Min. match in %")), 1, 100, false, &sT.compareSummaryMatchInPercent, rmsSearchTimerSave));
                }
                mainMenuItems.push_back(new cRecMenuItemSelect(cString::sprintf("%s%s%s%s", *indent, *indent, *indent, tr("Compare date")), compareDateModes, false, &sT.compareDate, rmsSearchTimerSave, false));
            }
            mainMenuItems.push_back(new cRecMenuItemInt(cString::sprintf("%s%s%s", *indent, *indent, tr("Priority")), 0, 99, false, &sT.priority, rmsSearchTimerSave));
            mainMenuItems.push_back(new cRecMenuItemInt(cString::sprintf("%s%s%s", *indent, *indent, tr("Lifetime")), 0, 99, false, &sT.lifetime, rmsSearchTimerSave));
            mainMenuItems.push_back(new cRecMenuItemInt(cString::sprintf("%s%s%s", *indent, *indent, tr("Time margin for start in minutes")), 0, 30, false, &sT.marginStart, rmsSearchTimerSave));
            mainMenuItems.push_back(new cRecMenuItemInt(cString::sprintf("%s%s%s", *indent, *indent, tr("Time margin for stop in minutes")), 0, 30, false, &sT.marginStop, rmsSearchTimerSave));
            mainMenuItems.push_back(new cRecMenuItemBool(cString::sprintf("%s%s%s", *indent, *indent, tr("Use VPS")), false, &sT.useVPS, rmsSearchTimerSave));
            mainMenuItems.push_back(new cRecMenuItemSelect(cString::sprintf("%s%s%s", *indent, *indent, tr("Auto delete")), delModes, false, &sT.delMode, rmsSearchTimerSave, true));
            if (sT.delMode == 1)
                mainMenuItems.push_back(new cRecMenuItemInt(cString::sprintf("%s%s%s%s", *indent, *indent, *indent, tr("after ... recordings")), 0, 999, false, &sT.delAfterCountRecs, rmsSearchTimerSave));
            else if (sT.delMode == 2)
                mainMenuItems.push_back(new cRecMenuItemInt(cString::sprintf("%s%s%s%s", *indent, *indent, *indent, tr("after ... days after first rec.")), 0, 999, false, &sT.delAfterDaysOfFirstRec, rmsSearchTimerSave));
        }
    }
    mainMenuItems.push_back(new cRecMenuItemButton(tr("Display Results for Search Timer"), rmsSearchTimerTest, false));

    bool reDraw = false;
    if (GetCurrentNumMenuItems() > 0) {
        InitMenu(false);
        reDraw = true;
    }
    
    numMenuItems = mainMenuItems.size();

    int start = GetStartIndex();
    for (int i = start; i < numMenuItems; i++) {
        if (!AddMenuItemInitial(mainMenuItems[i])) {
            break;
        }
    }
    if (reDraw)
        mainMenuItems[activeMenuItem]->SetActive();

    if (init) {
        init = !init;
    }
    CalculateHeight(!reDraw);
    CreatePixmap();
    Arrange();
}

cTVGuideSearchTimer cRecMenuSearchTimerEdit::GetSearchTimer(void) {
    sT.searchString = searchString;
    if (sT.useChannel == 1) {
#if VDRVERSNUM >= 20301
        {
        LOCK_CHANNELS_READ;
        sT.channelMin = Channels->GetByNumber(startChannel);
        sT.channelMax = Channels->GetByNumber(stopChannel);
        }
#else
        sT.channelMin = Channels.GetByNumber(startChannel);
        sT.channelMax = Channels.GetByNumber(stopChannel);
#endif
    }
    if (sT.useChannel == 2) {
        if (channelgroups.size() > 0) {
            std::string & channelGroup = channelgroups[channelgroupIndex];
            sT.channelGroup = channelGroup;
        } else {
            sT.useChannel = 0;
        }
    }
    if (sT.useDayOfWeek) {
        sT.dayOfWeek = SetDayOfWeek(dayOfWeek);
    }
    std::string dir(directory);
    std::replace(dir.begin(), dir.end(), '/', '~');
    sT.directory = dir;
    return sT;
}

int cRecMenuSearchTimerEdit::GetTotalNumMenuItems(void) { 
    return numMenuItems; 
}

cRecMenuItem *cRecMenuSearchTimerEdit::GetMenuItem(int number) {
    if ((number > -1) && (number < numMenuItems)) {
        return mainMenuItems[number];
    }
    return NULL;
}

// --- cRecMenuSearchTimerDeleteConfirm ---------------------------------------------
cRecMenuSearchTimerDeleteConfirm::cRecMenuSearchTimerDeleteConfirm(cTVGuideSearchTimer searchTimer) { // OK
    SetWidthPercent(70);

    this->searchTimer = searchTimer;

    const cString line1 = tr("Really delete Search Timer");
    const cString line2 = cString::sprintf("\"%s\"?", searchTimer.GetSearchString().c_str());

    AddHeader(new cRecMenuItemInfo(*line1, 2, *line2, "", "", width - 2 * border, true));
    AddFooter(new cRecMenuItemButton(tr("Cancel"), rmsClose, false));

    AddMenuItem(new cRecMenuItemButton(tr("Delete only Search Timer"), rmsSearchTimerDelete, true));
    AddMenuItem(new cRecMenuItemButton(tr("Delete Search Timer and created Timers"), rmsSearchTimerDeleteWithTimers, false));

    CalculateHeight();
    CreatePixmap();
    Arrange();
}

cTVGuideSearchTimer cRecMenuSearchTimerDeleteConfirm::GetSearchTimer(void) {
    return searchTimer;
}

// --- cRecMenuSearchTimerCreateConfirm  ---------------------------------------------------------
cRecMenuSearchTimerCreateConfirm::cRecMenuSearchTimerCreateConfirm(bool success) { // OK
    SetWidthPercent(50);

    int i = 1;
    cString line1 = "";
    cString line2 = "";
    if (success) {
        line1 = tr("Search Timer sucessfully created");
        line2 = tr("Search Timer update initialised");
        i = 2;
    } else {
        line1 = tr("Search Timer NOT sucessfully created");
    }

    AddHeader(new cRecMenuItemInfo(*line1, i, *line2, "", "", width - 2 * border));
    AddFooter(new cRecMenuItemButton(tr("OK"), rmsClose, true));

    CalculateHeight();
    CreatePixmap();
    Arrange();
}

// --- cRecMenuSearchTimerTemplatesCreate  ---------------------------------------------------------
cRecMenuSearchTimerTemplatesCreate::cRecMenuSearchTimerTemplatesCreate(TVGuideEPGSearchTemplate templ, cTVGuideSearchTimer searchTimer) { // OK
    SetWidthPercent(70);

    this->templ = templ;
    this->searchTimer = searchTimer;

    const cString line1 = tr("Creating Search Timer");
    const cString line2 = cString::sprintf("%s: \"%s\"", tr("Search Term"), searchTimer.GetSearchString().c_str());
    const cString line3 = cString::sprintf("%s \"%s\"", tr("Using Template"), templ.name.c_str());

    AddHeader(new cRecMenuItemInfo(*line1, 3, *line2, *line3, "", width - 2 * border));
    AddFooter(new cRecMenuItemButtonYesNo(tr("Create Search Timer"), tr("Use other Template"), rmsSearchTimerSave, rmsSearchTimerOptions, false));

    AddMenuItem(new cRecMenuItemButton(tr("Display Results for Search Timer"), rmsSearchTimerTest, true));

    CalculateHeight();
    CreatePixmap();
    Arrange();
}


// --- cRecMenuSearchTimerResults ---------------------------------------------------------
cRecMenuSearchTimerResults::cRecMenuSearchTimerResults(std::string searchString, const cEvent **searchResults, int numResults, std::string templateName, eRecMenuState action2) { // OK
    SetWidthPercent(70);

    this->searchResults = searchResults;
    this->action2 = action2;
    this->numResults = numResults;

    int i = (templateName.size() > 0) ? 3 : 2;
    const cString line1 = cString::sprintf("%d %s:", numResults, (numResults > 1) ?
                                                                 (action2 == rmsFavoritesRecord) ? tr("search results for Favorite")
                                                                                                 : tr("search results for Search Timer") :
                                                                 (action2 == rmsFavoritesRecord) ? tr("search result for Favorite")
                                                                                                 : tr("search result for Search Timer"));
    const cString line2 = cString::sprintf("\"%s\"", searchString.c_str());
    const cString line3 = (i == 3) ? cString::sprintf("%s \"%s\"", tr("Using Template"), templateName.c_str()) : "";

    AddHeader(new cRecMenuItemInfo(*line1, i, *line2, *line3, "", width - 2 * border, true));
    AddFooter(new cRecMenuItemButton(tr("Close"), rmsClose, false));

    if (searchResults && (numResults > 0)) {
        for (int i = 0; i < numResults; i++) {
            if (!AddMenuItemInitial(new cRecMenuItemEvent(searchResults[i], rmsSearchShowInfo, action2, (i == 0) ? true : false)))
                break;
        }
    }

    CalculateHeight();
    CreatePixmap();
    Arrange();
}
    
cRecMenuItem *cRecMenuSearchTimerResults::GetMenuItem(int number) { 
    if ((number >= 0) && (number < numResults)) {
        if (cRecMenuItem *result = new cRecMenuItemEvent(searchResults[number], rmsSearchShowInfo, action2, false))
            return result;
    }
    return NULL;
}

int cRecMenuSearchTimerResults::GetTotalNumMenuItems(void) { 
    return numResults;
}

const cEvent *cRecMenuSearchTimerResults::GetEvent(void) {
    if (cRecMenuItemEvent *activeItem = dynamic_cast<cRecMenuItemEvent*>(GetActiveMenuItem()))
        return activeItem->GetEvent();
    return NULL;
}

// --- cRecMenuSearchTimerNothingFound  ---------------------------------------------------------
cRecMenuSearchTimerNothingFound::cRecMenuSearchTimerNothingFound(std::string searchString) { // OK
    SetWidthPercent(50);

    const cString line1 = tr("Nothing found for Search String");
    const cString line2 = cString::sprintf("\"%s\"", searchString.c_str());

    AddHeader(new cRecMenuItemInfo(*line1, 2, *line2, "", "", width - 2 * border));
    AddFooter(new cRecMenuItemButton(tr("OK"), rmsClose, true, true));

    CalculateHeight();
    CreatePixmap();
    Arrange();
}

/******************************************************************************************
*   SwitchTimer Menus
******************************************************************************************/

// --- cRecMenuSwitchTimer  ---------------------------------------------------------
cRecMenuSwitchTimer::cRecMenuSwitchTimer(void) { // OK
    SetWidthPercent(60);

    switchMinsBefore = config.switchMinsBefore;
    switchMode = config.switchMode;

    AddHeader(new cRecMenuItemInfo(tr("Configure Options for Switchtimer"), 1, "", "", "", width - 2 * border, true));
    AddFooter(new cRecMenuItemButtonYesNo(tr("Create"), tr("Cancel"), rmsSwitchTimerCreate, rmsClose, true));

    std::vector<std::string> switchModeItems;
    switchModeItems.push_back(tr("switch"));
    switchModeItems.push_back(tr("announce only"));
    switchModeItems.push_back(tr("ask for switch"));

    AddMenuItem(new cRecMenuItemInt(tr("Minutes before switching"), 0, 10, false, &switchMinsBefore));
    AddMenuItem(new cRecMenuItemSelect(tr("Switch Mode"), switchModeItems, false, &switchMode));

    CalculateHeight();
    CreatePixmap();
    Arrange();
}

cSwitchTimer cRecMenuSwitchTimer::GetSwitchTimer(void) {
    cSwitchTimer st;
    st.switchMinsBefore = switchMinsBefore;
    st.switchMode = switchMode;
    return st;
}

// --- cRecMenuSwitchTimerConfirm  ---------------------------------------------------------
cRecMenuSwitchTimerConfirm::cRecMenuSwitchTimerConfirm(bool success) { // OK
    SetWidthPercent(50);

    const cString line1 = (success) ? tr("Switch Timer sucessfully created") : tr("Switch Timer NOT sucessfully created");

    AddHeader(new cRecMenuItemInfo(*line1, 1, "", "", "", width - 2 * border));
    AddFooter(new cRecMenuItemButton(tr("OK"), rmsClose, true));

    CalculateHeight();
    CreatePixmap();
    Arrange();
}

// --- cRecMenuSwitchTimerDelete  ---------------------------------------------------------
cRecMenuSwitchTimerDelete::cRecMenuSwitchTimerDelete(void) { // OK
    SetWidthPercent(50);

    const cString line1 = tr("Switch Timer deleted");

    AddHeader(new cRecMenuItemInfo(*line1, 1, "", "", "", width - 2 * border));
    AddFooter(new cRecMenuItemButton(tr("OK"), rmsClose, true));

    CalculateHeight();
    CreatePixmap();
    Arrange();
}

/******************************************************************************************
*   Search Menus
******************************************************************************************/


// --- cRecMenuSearch  ---------------------------------------------------------
cRecMenuSearch::cRecMenuSearch(std::string searchString, bool withOptions) { // OK
    SetWidthPercent(60);

    strncpy(this->searchString, searchString.c_str(), TEXTINPUTLENGTH);
    mode = 0;
    channelNr = 0;
    useTitle = true;
    useSubTitle = true;
    useDescription = false;

    const cString line1 = tr("Search");

    AddHeader(new cRecMenuItemInfo(*line1, 1, "", "", "", width - 2 * border, true));
    AddFooter(new cRecMenuItemButtonYesNo(tr("Perform Search"), tr("Cancel"), rmsSearchPerform, rmsClose, true));

    AddMenuItem(new cRecMenuItemText(tr("Search Expression:"), TEXTINPUTLENGTH, false, this->searchString));

    if (withOptions) {
        std::vector<std::string> searchModes;
        cTVGuideSearchTimer searchTimer;
        searchTimer.GetSearchModes(&searchModes);
        AddMenuItem(new cRecMenuItemSelect(tr("Search Mode"), searchModes, false, &mode));
        AddMenuItem(new cRecMenuItemChannelChooser(tr("Channel to Search"), false, &channelNr));
        AddMenuItem(new cRecMenuItemBool(tr("Search in title"), false, &useTitle));
        AddMenuItem(new cRecMenuItemBool(tr("Search in Subtitle"), false, &useSubTitle));
        AddMenuItem(new cRecMenuItemBool(tr("Search in Description"), false, &useDescription));
    } else {
        AddMenuItem(new cRecMenuItemButton(tr("Show Search Options"), rmsSearchWithOptions, false));
    }

    CalculateHeight();
    CreatePixmap();
    Arrange();
}

Epgsearch_searchresults_v1_0 cRecMenuSearch::GetEPGSearchStruct(void) {
    Epgsearch_searchresults_v1_0 data;
    data.query = searchString;
    data.mode = mode;
    data.channelNr = channelNr;
    data.useTitle = useTitle;
    data.useSubTitle = useSubTitle;
    data.useDescription = useDescription;
    return data;
}


// --- cRecMenuSearchResults  ---------------------------------------------------------
cRecMenuSearchResults::cRecMenuSearchResults(std::string searchString, const cEvent **searchResults, int numResults) { // OK
    SetWidthPercent(70);

    this->searchString = searchString;
    this->searchResults = searchResults;
    this->numResults = numResults;

    const cString line1 = cString::sprintf("%d %s:", numResults, (numResults > 1) ? tr("search results for") : tr("search result for"));
    const cString line2 = cString::sprintf("\"%s\"", searchString.c_str());

    AddHeader(new cRecMenuItemInfo(*line1, 2, *line2, "", "", width - 2 * border, true));
    AddFooter(new cRecMenuItemButtonYesNo(tr("Adapt Search"), tr("Close"), rmsSearch, rmsClose, false));

    if (searchResults && (numResults > 0)) {
        for (int i = 0; i < numResults; i++) {
            if (!AddMenuItemInitial(new cRecMenuItemEvent(searchResults[i], rmsSearchShowInfo, rmsSearchRecord, (i == 0) ? true : false)))
                break;
        }
    }

    CalculateHeight();
    CreatePixmap();
    Arrange();
}

cRecMenuItem *cRecMenuSearchResults::GetMenuItem(int number) {
    if ((number >= 0) && (number < numResults)) {
        cRecMenuItem *result = new cRecMenuItemEvent(searchResults[number], rmsSearchShowInfo, rmsSearchRecord, false);
        return result;
    }
    return NULL;
}

int cRecMenuSearchResults::GetTotalNumMenuItems(void) { 
    return numResults;
}

const cEvent *cRecMenuSearchResults::GetEvent(void) {
    if (cRecMenuItemEvent *activeItem = dynamic_cast<cRecMenuItemEvent*>(GetActiveMenuItem()))
        return activeItem->GetEvent();
    return NULL;
}

// --- cRecMenuSearchNothingFound  ---------------------------------------------------------
cRecMenuSearchNothingFound::cRecMenuSearchNothingFound(std::string searchString, bool tooShort) { // OK
    SetWidthPercent(50);

    const cString line1 = cString::sprintf("%s", (!tooShort) ? tr("Nothing found for Search String") : tr("Search String has to have at least three letters"));
    const cString line2 = cString::sprintf("\"%s\"", searchString.c_str());

    AddHeader(new cRecMenuItemInfo(*line1, 2, *line2, "", "", width - 2 * border));
    AddFooter(new cRecMenuItemButton(tr("OK"), rmsSearchNothingFoundConfirm, true, true));

    CalculateHeight();
    CreatePixmap();
    Arrange();
}

// --- cRecMenuSearchConfirmTimer  ---------------------------------------------------------
cRecMenuSearchConfirmTimer::cRecMenuSearchConfirmTimer(const cEvent *event, eRecMenuState nextAction) { // OK
    SetWidthPercent(50);

    const cChannels *channels = NULL;
#if VDRVERSNUM >= 20301
    {
    LOCK_CHANNELS_READ;
    channels = Channels;
    }
#else
    channels = &Channels;
#endif
    const cString channelName = channels->GetByChannelID(event->ChannelID())->Name();

    const cString line1 = tr("Timer created");
    const cString line3 = cString::sprintf("%s %s - %s", *event->GetDateString(), *event->GetTimeString(), *event->GetEndTimeString());
    const cString line4 = (event && event->Title()) ? cString::sprintf("\"%s\"", event->Title()) : "";

    AddHeader(new cRecMenuItemInfo(*line1, 4, *channelName, *line3, *line4, width - 2 * border));
    AddFooter(new cRecMenuItemButton(tr("OK"), nextAction, true, true));

    CalculateHeight();
    CreatePixmap();
    Arrange();
}

/******************************************************************************************
*   Timeline
******************************************************************************************/

// --- cRecMenuTimeline  ---------------------------------------------------------
cRecMenuTimeline::cRecMenuTimeline(cTVGuideTimerConflicts *timerConflicts) { // OK
    SetWidthPercent(90);

    numTimersToday = 0;
    this->timerConflicts = timerConflicts;
    SetStartStop();
    conflictsToday = timerConflicts->GetConflictsBetween(timeStart, timeStop);
    GetTimersForDay();

    header = new cRecMenuItemTimelineHeader(timeStart, numTimersToday, conflictsToday);
    AddHeader(header);

    footer = new cRecMenuItemButton(tr("Close"), rmsClose, false, true);
    AddFooter(footer);

    SetTimers();
}

void cRecMenuTimeline::SetStartStop(void) {
    time_t now = time(0);
    tm *timeStruct = localtime(&now);
    timeStart = now - timeStruct->tm_hour * 3600 - timeStruct->tm_min * 60 - timeStruct->tm_sec;
    today = timeStart;
    timeStop = timeStart + 24 * 3600 - 1;
}

void cRecMenuTimeline::GetTimersForDay(void) {
    timersToday.clear();
#if VDRVERSNUM >= 20301
    LOCK_TIMERS_READ;
    const cTimers* timers = Timers;
    cSortedTimers SortedTimers(timers);
    int i = 0;
    while (i < SortedTimers.Size()) {
        const cTimer *t = SortedTimers[i];
        if (((t->StartTime() > timeStart) && (t->StartTime() <= timeStop)) || ((t->StopTime() > timeStart) && (t->StopTime() <= timeStop))) {
            if (t->HasFlags(tfActive))
                timersToday.push_back(t);
        }
        i++;
    }
#else
    for (const cTimer *t = Timers.First(); t; t = Timers.Next(t)) {
        if (((t->StartTime() > timeStart) && (t->StartTime() <= timeStop)) || ((t->StopTime() > timeStart) && (t->StopTime() <= timeStop))) {
            timersToday.push_back(t);
        }
    }
#endif
    numTimersToday = timersToday.size();
}

void cRecMenuTimeline::SetTimers(void) {
    ClearMenu();
    GetTimersForDay();
    conflictsToday = timerConflicts->GetConflictsBetween(timeStart, timeStop);
    header->UnsetCurrentTimer();
    header->SetDay(timeStart);
    header->RefreshTimerDisplay();
    header->SetNumTimersToday(numTimersToday);
    if (numTimersToday == 0) {
        AddMenuItem(new cRecMenuItemTimelineTimer(NULL, 0, 0, conflictsToday, header, false));
        footer->SetActive();
    } else {
        for (int i = 0; i < numTimersToday; i++) {
            if (!AddMenuItemInitial(new cRecMenuItemTimelineTimer(timersToday[i], timeStart, timeStop, conflictsToday, header, (i == 0) ? true : false)))
                break;
        }
        footer->setInactive();
    }

    CalculateHeight();
    CreatePixmap();
    Arrange();
}

void cRecMenuTimeline::PrevDay(void) {
    if ((timeStart - 3600 * 24) < today)
        return;
    timeStart -= 3600 * 24;
    timeStop  -= 3600 * 24;
    SetTimers();
    Display();
}

void cRecMenuTimeline::NextDay(void) {
    timeStart += 3600 * 24;
    timeStop  += 3600 * 24;
    SetTimers();
    Display();
}

cRecMenuItem *cRecMenuTimeline::GetMenuItem(int number) { 
    if ((number < 0) || (number >= numTimersToday))
        return NULL;
    return new cRecMenuItemTimelineTimer(timersToday[number], timeStart, timeStop, conflictsToday, header, false);
}

int cRecMenuTimeline::GetTotalNumMenuItems(void) { 
    return numTimersToday;
}

void cRecMenuTimeline::ClearMenu(void) {
    InitMenu(true);
    header->UnsetCurrentTimer();
}

const cTimer *cRecMenuTimeline::GetTimer(void) {
    if (cRecMenuItemTimelineTimer *activeItem = dynamic_cast<cRecMenuItemTimelineTimer*>(GetActiveMenuItem()))
        return activeItem->GetTimer();
    return NULL;
}

eRecMenuState cRecMenuTimeline::ProcessKey(eKeys Key) {
    eRecMenuState state = rmsContinue;
    switch (Key & ~k_Repeat) {
        case kLeft:
            PrevDay();
            state = rmsConsumed;
            break;
        case kRight:
            NextDay();
            state = rmsConsumed;
            break;
        default:
            break;
    }
    if (state != rmsConsumed) {
        state = cRecMenu::ProcessKey(Key);
    }
    return state;
}

/******************************************************************************************
*   Recording Search Menus
******************************************************************************************/

// --- cRecMenuRecordingSearch  --------------------------------------------------------- // OK
cRecMenuRecordingSearch::cRecMenuRecordingSearch(std::string search) {
    SetWidthPercent(60);

    const cString line1 = tr("Search in Recordings");

    AddHeader(new cRecMenuItemInfo(*line1, 1, "", "", "", width - 2 * border, true));
    AddFooter(new cRecMenuItemButtonYesNo(tr("Perform Search"), tr("Cancel"), rmsRecordingSearchResult, rmsClose, true));

    strncpy(searchString, search.c_str(), TEXTINPUTLENGTH);
    AddMenuItem(new cRecMenuItemText(tr("Search Expression:"), TEXTINPUTLENGTH, false, searchString));

    CalculateHeight();
    CreatePixmap();
    Arrange();
}

// --- cRecMenuRecordingSearchResults  ---------------------------------------------------------
cRecMenuRecordingSearchResults::cRecMenuRecordingSearchResults(std::string searchString, const cRecording **searchResults, int numResults) { // OK
    SetWidthPercent(80);

    this->searchString = searchString;
    this->searchResults = searchResults;
    this->numResults = numResults;

    const cString line1 = cString::sprintf("%s %d %s %s:", tr("Found"), numResults, (numResults > 1) ? tr("recordings") : tr("recording"), tr("for"));
    const cString line2 = cString::sprintf("\"%s\"", searchString.c_str());

    AddHeader(new cRecMenuItemInfo(*line1, 2, *line2, "", "", width - 2 * border, true));
    AddFooter(new cRecMenuItemButtonYesNo(tr("Adapt Search"), tr("Close"), rmsRecordingSearch, rmsClose, (numResults) ? false : true));

    if (searchResults && (numResults > 0)) {
        for (int i = 0; i < numResults; i++) {
            if (!AddMenuItemInitial(new cRecMenuItemRecording(searchResults[i], (i == 0) ? true : false)))
                break;
        }
    }

    CalculateHeight();
    CreatePixmap();
    Arrange();
}

cRecMenuItem *cRecMenuRecordingSearchResults::GetMenuItem(int number) {
    if ((number >= 0) && (number < numResults)) {
        cRecMenuItem *result = new cRecMenuItemRecording(searchResults[number], false);
        return result;
    }
    return NULL;
}

int cRecMenuRecordingSearchResults::GetTotalNumMenuItems(void) {
    return numResults;
}

// --- cRecMenuRecordingSearchNotFound  ---------------------------------------------------------
cRecMenuRecordingSearchNotFound::cRecMenuRecordingSearchNotFound(std::string searchString) { // OK
    SetWidthPercent(50);

    const cString line1 = cString::sprintf("%s", tr("No recordings found for"));
    const cString line2 = cString::sprintf("\"%s\"", searchString.c_str());

    AddHeader(new cRecMenuItemInfo(tr("No recordings found for"), 2, *line2, "", "", width - 2 * border));
    AddFooter(new cRecMenuItemButton(tr("OK"), rmsClose, true, true));

    CalculateHeight();
    CreatePixmap();
    Arrange();
}

/******************************************************************************************
*   Favorites
******************************************************************************************/

// --- cRecMenuFavorites  ---------------------------------------------------------
cRecMenuFavorites::cRecMenuFavorites(std::vector<cTVGuideSearchTimer> favorites) { // OK
    SetWidthPercent(70);

    deleteMenuItems = false;
    this->favorites = favorites;

    const cString line1 = cString::sprintf("%s", (numFavorites > 0) ? tr("Favorites") : tr("No Favorites available"));

    AddHeader(new cRecMenuItemInfo(*line1, 1, "", "", "", width - 2 * border, true));

    CreateFavoritesMenuItems();

// AddFooter has to be after CreateFavoritesMenuItems because of numFavorites
    AddFooter(new cRecMenuItemButton(tr("Close"), rmsClose, (numFavorites) ? false : true));

    for (int i = 0; i < numFavorites; i++) {
        if (i == 0)
            myMenuItems[i]->SetActive();
        if (!AddMenuItemInitial(myMenuItems[i]))
            break;
    }

    CalculateHeight();
    CreatePixmap();
    Arrange();
}

cRecMenuFavorites::~cRecMenuFavorites(void) {
    for (std::vector<cRecMenuItem*>::iterator it = myMenuItems.begin(); it != myMenuItems.end(); it++) {
        delete *it;
    }
    myMenuItems.clear();
}

void cRecMenuFavorites::CreateFavoritesMenuItems(void) {
    if (config.favWhatsOnNow) {
        myMenuItems.push_back(new cRecMenuItemFavoriteStatic(tr("What's on now"), rmsFavoritesNow, false));
    }
    if (config.favWhatsOnNext) {
        myMenuItems.push_back(new cRecMenuItemFavoriteStatic(tr("What's on next"), rmsFavoritesNext, false));
    }
    if (config.favUseTime1) {
        std::string desc = *cString::sprintf("%s (%s)", config.descUser1.c_str(), NiceTime(config.favTime1).c_str());
        myMenuItems.push_back(new cRecMenuItemFavoriteStatic(desc, rmsFavoritesUser1, false));
    }
    if (config.favUseTime2) {
        std::string desc = *cString::sprintf("%s (%s)", config.descUser2.c_str(), NiceTime(config.favTime2).c_str());
        myMenuItems.push_back(new cRecMenuItemFavoriteStatic(desc, rmsFavoritesUser2, false));
    }
    if (config.favUseTime3) {
        std::string desc = *cString::sprintf("%s (%s)", config.descUser3.c_str(), NiceTime(config.favTime3).c_str());
        myMenuItems.push_back(new cRecMenuItemFavoriteStatic(desc, rmsFavoritesUser3, false));
    }
    if (config.favUseTime4) {
        std::string desc = *cString::sprintf("%s (%s)", config.descUser4.c_str(), NiceTime(config.favTime4).c_str());
        myMenuItems.push_back(new cRecMenuItemFavoriteStatic(desc, rmsFavoritesUser4, false));
    }

    int numAdditionalFavs = favorites.size();
    for (int i = 0; i < numAdditionalFavs; i++) {
        myMenuItems.push_back(new cRecMenuItemFavorite(favorites[i], rmsSearchTimerTest, false));
    }
    numFavorites = myMenuItems.size();
}

std::string cRecMenuFavorites::NiceTime(int favTime) {
    int hours = favTime / 100;
    int mins = favTime - hours * 100;
    return *cString::sprintf("%02d:%02d", hours, mins);
}

cRecMenuItem *cRecMenuFavorites::GetMenuItem(int number) {
    if (number > -1 && number < numFavorites)
        return myMenuItems[number];
    return NULL;
}

int cRecMenuFavorites::GetTotalNumMenuItems(void) {
    return numFavorites;
}

cTVGuideSearchTimer cRecMenuFavorites::GetFavorite(void) {
    cRecMenuItemFavorite *activeItem = dynamic_cast<cRecMenuItemFavorite*>(GetActiveMenuItem());
    return activeItem->GetFavorite();
}
