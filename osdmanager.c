#include "config.h"
#include "osdmanager.h"

cOsdManager::cOsdManager(void) {
    osd = NULL;
}

void cOsdManager::Lock(void) {
    mutex.Lock();
}

void cOsdManager::Unlock(void) {
    mutex.Unlock();
}

bool cOsdManager::CreateOsd(void) {
    osd = cOsdProvider::NewOsd(cOsd::OsdLeft(), cOsd::OsdTop());
    if (osd) {
        tArea Area = { 0, 0, cOsd::OsdWidth() - 1, cOsd::OsdHeight() - 1,  32 };
        if (osd->SetAreas(&Area, 1) == oeOk) {  
            return true;
        }
    }
    return false;
}

void cOsdManager::DeleteOsd(void) {
    Lock();
    delete osd;
    osd = NULL;
    Unlock();
}

void cOsdManager::SetBackground(void) {
    
    if (config.displayStatusHeader && config.scaleVideo) {
        int widthStatus = cOsd::OsdWidth() - geoManager.statusHeaderHeight * 16 / 9;
        osd->DrawRectangle(0, 0, widthStatus, geoManager.statusHeaderHeight, theme.Color(clrBackgroundOSD));
        osd->DrawRectangle(0, geoManager.statusHeaderHeight, Width(), Height(), theme.Color(clrBackgroundOSD));    
    }
    else
        osd->DrawRectangle(0, 0, Width(), Height(), theme.Color(clrBackgroundOSD));
    
}

cPixmap *cOsdManager::CreatePixmap(int Layer, const cRect &ViewPort, const cRect &DrawPort) {
    if (osd)
        return osd->CreatePixmap(Layer, ViewPort, DrawPort);
    return NULL;
}

void cOsdManager::DestroyPixmap(cPixmap *pixmap) {
    if (!osd || !pixmap)
        return;
    osd->DestroyPixmap(pixmap);
}

void cOsdManager::Flush(void) {
    if (osd) {
        osd->Flush();
    }
}
