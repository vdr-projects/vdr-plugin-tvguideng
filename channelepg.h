#ifndef __TVGUIDE_CHANNELEPG_H
#define __TVGUIDE_CHANNELEPG_H

#include <vdr/tools.h>
#include "gridelement.h"
#include "epggrid.h"
#include "headergrid.h"
#include "switchtimer.h"

class cGridElement;
class cEpgGrid;
class cHeaderGrid;

// --- cChannelEpg  -------------------------------------------------------------

class cChannelEpg : public cListObject, public cStyledPixmap {
private:
    cTimeManager *timeManager;
    int num;
    const cChannel *channel;
    cHeaderGrid *header;
    cList<cGridElement> grids;
#if VDRVERSNUM >= 20301
#else
    cSchedulesLock *schedulesLock;
#endif
    const cSchedules *schedules;
    bool hasTimer;
    bool hasSwitchTimer;
    cGridElement *addEpgGrid(const cEvent *event, cGridElement *firstGrid, bool color);
    cGridElement *addDummyGrid(time_t start, time_t end, cGridElement *firstGrid, bool color);
public:
    cChannelEpg(int num, const cChannel *channel, cTimeManager *timeManager);
    virtual ~cChannelEpg(void);
    void createHeader();
    void drawHeader();
    bool readGrids();
    void drawGrids();
    int getX();
    int getY();
    int Start() { return timeManager->GetStart(); };
    int Stop()  { return timeManager->GetEnd(); };
    const char* Name() { return channel->Name(); };
    const cChannel *getChannel() {return channel;}
    cGridElement *getActive(bool last = false);
    cGridElement *getNext(cGridElement *activeGrid);
    cGridElement *getPrev(cGridElement *activeGrid);
    cGridElement *getNeighbor(cGridElement *activeGrid);
    bool isFirst(cGridElement *grid);
    void AddNewGridsAtStart();
    void AddNewGridsAtEnd();
    void ClearOutdatedStart();
    void ClearOutdatedEnd();
    int GetNum() {return num;};
    void SetNum(int num) {this->num = num;};
#if VDRVERSNUM >= 20301
    void setTimer();
#else
    void setTimer() {hasTimer = channel->HasTimer();};
#endif
    bool HasTimer() { return hasTimer; };
    void setSwitchTimer() {hasSwitchTimer = SwitchTimers.ChannelInSwitchList(channel);};
    bool HasSwitchTimer() { return hasSwitchTimer; };
    void SetTimers();
    void clearGrids();
    void dumpGrids();
};

#endif //__TVGUIDE_CHANNELEPG_H
