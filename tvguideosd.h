#ifndef __TVGUIDE_TVGUIDEOSD_H
#define __TVGUIDE_TVGUIDEOSD_H

#include "timemanager.h"
#include "gridelement.h"
#include "channelepg.h"
#include "statusheader.h"
#include "detailview.h"
#include "timeline.h"
#include "channelgroups.h"
#include "footer.h"
#include "recmenuview.h"
#include "channeljump.h"

// --- cTvGuideOsd -------------------------------------------------------------

class cTvGuideOsd : public cOsdObject {
private:
  cTimeManager *timeManager;
  cList<cChannelEpg> columns;
  cGridElement *activeGrid;
  cStatusHeader *statusHeader;
  cDetailView *detailView;
  cTimeLine *timeLine;
  cChannelGroups *channelGroups;
  cFooter *footer;
  cRecMenuView *recMenuView;
  cChannelJump *channelJumper;
  int GetLastValidChannel(void);
  bool detailViewActive;
  void drawOsd();
  void readChannels(const cChannel *channelStart);
  void drawGridsChannelJump(int offset = 0);
  void drawGridsTimeJump(bool last = false);
  void processKeyUp();
  void processKeyDown();
  void processKeyLeft();
  void processKeyRight();
  void processKeyRed();
  void processKeyGreen();
  void processKeyYellow();
  eOSState processKeyBlue(bool *alreadyUnlocked);
  eOSState processKeyOk(bool *alreadyUnlocked);
  void processNumKey(int numKey);
  void TimeJump(int mode);
  void ChannelJump(int num);
  void CheckTimeout(void);
  void setNextActiveGrid(cGridElement *next);
  void channelForward();
  void channelBack();
  void timeForward();
  void timeBack();
  void ScrollForward();
  void ScrollBack();
  eOSState ChannelSwitch(bool *alreadyUnlocked);
  void DetailedEPG();
  void SetTimers();
  void dump();
public:
  cTvGuideOsd(void);
  virtual ~cTvGuideOsd(void);
  virtual void Show(void);
  virtual eOSState ProcessKey(eKeys Key);
};

#endif //__TVGUIDE_TVGUIDEOSD_H
